﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingPercents : MonoBehaviour
{
    
    private float loadingTimes;
   //  public GameObject nextObject;
    public Text text;
    private float startTime;
    private float endTime;
    private float delta;
    private float currentTime;

    private AsyncOperation operation;

    void Start() {
  
    }

	void OnEnable () {
		LoadMainScene ();
	}

	public void LoadMainScene () {
		operation = SceneManager.LoadSceneAsync("main_new");

		currentTime = startTime = Time.time;
		text.text = "0%";
	}

    void FixedUpdate()
    {
        text.text = Mathf.RoundToInt(operation.progress * 100) + "%";
    }


}
