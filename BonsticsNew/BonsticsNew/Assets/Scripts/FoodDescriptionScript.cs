﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodDescriptionScript : MonoBehaviour {

    void Awake()
    {
        ItemInFridgeScript.OnClickFood += OnClickFood_Event;
        
    }

    void OnClickFood_Event(Sprite sprite)
    {
        ItemImageScript itemImageScript =  gameObject.GetComponentInChildren<ItemImageScript>();

        Image image =  itemImageScript.gameObject.GetComponent<Image>();
        image.sprite = sprite;
    }
}
