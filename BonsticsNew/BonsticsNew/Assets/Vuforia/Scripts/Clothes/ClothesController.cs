﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ClothesController : MonoBehaviour
{
    public static event Action LeftButtonClicked;
    public static event Action RightButtonClicked;
    public static event Action ClothesButtonClicked;
    public static event Action BoardsButtonClicked;
    public static event Action AccessoriesButtonClicked;
    public Text text;

    public Button Left;
    public Button Right;

    public Button Boards;
    public Button Clothes;
    public Button Accessories;

    void Start()
    {        
        var leftPressed = new Button.ButtonClickedEvent();
        leftPressed.AddListener(LeftButton_pressed);
        Left.onClick = leftPressed;

        var rightPressed = new Button.ButtonClickedEvent();
        rightPressed.AddListener(RightButton_pressed);
        Right.onClick = rightPressed;

        var clothesPressed = new Button.ButtonClickedEvent();
        clothesPressed.AddListener(ClothesButton_pressed);
        Clothes.onClick = clothesPressed;

        var accessoriesPressed = new Button.ButtonClickedEvent();
        accessoriesPressed.AddListener(AccessoriesButton_pressed);
        Accessories.onClick = accessoriesPressed;

        var boardsPressed = new Button.ButtonClickedEvent();
        boardsPressed.AddListener(BoardsButton_pressed);
        Boards.onClick = boardsPressed;

        ChangeClothesScript.ItemChanged += Item_Changed_handler;
    }

    void Item_Changed_handler(string name)
    {
        text.text = name;
    }


    void AccessoriesButton_pressed()
    {
        if (AccessoriesButtonClicked != null)
            AccessoriesButtonClicked();
    }

    void BoardsButton_pressed()
    {
        if (BoardsButtonClicked != null)
            BoardsButtonClicked();
    }

    void ClothesButton_pressed()
    {
        if (ClothesButtonClicked != null)
            ClothesButtonClicked();
    }

    void RightButton_pressed()
    {
        if (RightButtonClicked != null)
        {
            RightButtonClicked();
        }
    }

    void LeftButton_pressed()
    {
        if (LeftButtonClicked != null)
        {
            LeftButtonClicked();
        }
    }
}
