﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
public class FridgeItem
{
    public Sprite sprite { get; set; }
    public int numberInFridge;
}
*/

public class FridgeManager : MonoBehaviour
{
    public GameObject item;
    public GameObject[] placeholders;
    private Stack<GameObject> itemsInFridge;
    private Sprite[] sprites;

    void Start()
    {
        itemsInFridge = new Stack<GameObject>();
        Object[] textures = Resources.LoadAll("Food icons/png", typeof(Sprite));

        FillSprites(textures);

        FillFridge();
    }

    private void FillFridge()
    {
        int randomQuantity = Random.Range(5, 17);

        for (int i = 0; i < randomQuantity; i++)
        {
            GameObject newItem = Instantiate(item, placeholders[i].transform, false);

            itemsInFridge.Push(newItem);

            Image[] images = newItem.GetComponentsInChildren<Image>();
            images[1].sprite = sprites[Random.Range(0, sprites.Length)];
            Text[] text = newItem.GetComponentsInChildren<Text>();
            text[0].text = Random.Range(1, 13).ToString();
        }
    }

    private void FillSprites(Object[] textures)
    {
        sprites = new Sprite[textures.Length];

        for (int i = 0; i < textures.Length; i++)
        {
            sprites[i] = textures[i] as Sprite;
        }
    }
}
