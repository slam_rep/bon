﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ProfileScript : MonoBehaviour
{
    public Button closeButton;
    public Button getOutFromAcc;
    public GameObject PopUpClose;
    public GameObject PopUpGetOutFromAcc;

    void Start()
    {
        TurnOffPopUp(PopUpClose);
        TurnOffPopUp(PopUpGetOutFromAcc);

//        var clickClose = new Button.ButtonClickedEvent();
//        clickClose.AddListener(clickClose_handler);
//        closeButton.onClick = clickClose;

        var clickCloseAcc = new Button.ButtonClickedEvent();
        clickCloseAcc.AddListener(clickCloseAcc_handler);
		getOutFromAcc.onClick = clickCloseAcc;
    }

    private void clickCloseAcc_handler()
    {
        PopUpGetOutFromAcc.SetActive(true);
    }

    public void TurnOffPopUp(GameObject PopUp)
    {
        PopUp.SetActive(false);
    }

	public void LogoutConfirmClicked (){
        Destroy(ScreenManager.I);
		PlayerPrefs.DeleteKey (WebRequestController.PPKey_TOKEN);
		SceneManager.LoadSceneAsync("Loading");
	}

	public void CollectionsClicked () {
		ScreenManager.I.OpenScene("CollectionScene");
	}

    private void TurnOnPopUp(GameObject PopUp)
    {
        PopUpClose.SetActive(true);
    }

    private void clickClose_handler()
    {
        PopUpClose.SetActive(true);
    }
}
