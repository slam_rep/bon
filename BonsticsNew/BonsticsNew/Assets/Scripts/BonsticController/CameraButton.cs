﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraButton : MonoBehaviour {



	// Use this for initialization
	void OnEnable()
	{
	    Button but =  GetComponent<Button>();

        var clickEvent = new Button.ButtonClickedEvent();
        clickEvent.AddListener(clickEvent_handler);
	    but.onClick = clickEvent;
	}

    void clickEvent_handler()
    {
        ScreenManager.I.OpenScene("CatchScene");
    }
	
	
}
