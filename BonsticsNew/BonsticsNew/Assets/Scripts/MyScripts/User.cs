﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;

public class User : MonoBehaviour
{
    public int IdUser { get; private set; }
    public string NickName;
    public string Name;
    public string Surname;
    public string LoyalityCard;
    public string City;

    public int Traps;
    private int coins;
    public int eCoins
    {
        get { return this.coins; }
        set
        {
            this.coins = value;
            if (changeCoins != null)
                changeCoins();
        }
    }


    public event System.Action changeCoins;
	// Use this for initialization
	void Start ()
	{
        GameObject.DontDestroyOnLoad(gameObject);

        if(IdUser == 0)
	    IdUser = Random.Range(int.MinValue, int.MaxValue);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    
}
