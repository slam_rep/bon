﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EarnPopUp : MonoBehaviour
{
    public Text text;


	// Use this for initialization
	void Start ()
	{
        if(SceneManager.GetActiveScene().name == "FlappyCube")
	    text.text = (int) (FlappyCube.score / 5) + "";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Close()
    {
        ScreenManager.I.OpenScene("main_new", "MinGames");
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
