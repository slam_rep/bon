﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonsticItemControl : MonoBehaviour
{

    public Image[] images;
    public Text text;
    private string[] imagesNames;

    void OnEnabled()
    {
        images = new Image[3];
        imagesNames = new String[3];
    }
}
