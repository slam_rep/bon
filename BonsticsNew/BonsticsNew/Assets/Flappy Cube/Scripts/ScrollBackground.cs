﻿using UnityEngine;
using System.Collections;

public class ScrollBackground : MonoBehaviour
{

    //variables visible in the inspector
    public float scrollSpeed;
    public float Width;
    public float Height;
    public bool scrollVertical;

    void Start()
    {
        //get screen width/height and adjust background size to that
        float height = (float)Camera.main.orthographicSize * 2.0f;
        float width = height * Screen.width / Screen.height;
        transform.localScale = new Vector3(width * Width, height * Height, 1);
    }

    void Update()
    {
        //scroll background vertical if you have scrollVertical checked
        if (scrollVertical)
        {
            Vector2 offset = new Vector2(0, Time.time * -scrollSpeed);
            gameObject.GetComponent<Renderer>().material.mainTextureOffset = offset;
        }
        else
        {
            //else, scroll horizontal
            Vector2 offset = new Vector2(Time.time * scrollSpeed, 0);
            gameObject.GetComponent<Renderer>().material.mainTextureOffset = offset;
        }
    }
}
