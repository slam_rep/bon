﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveFoodDescriptionScript : MonoBehaviour
{

    public GameObject obj;

    public static ActiveFoodDescriptionScript I { get; private set; }

    void Start()
    {
        I = this;
    }

    public void SetActiveScreen(bool isActive)
    {
        obj.SetActive(isActive);
    }
}
