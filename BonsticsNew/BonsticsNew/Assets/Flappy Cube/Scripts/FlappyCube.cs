﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FlappyCube : MonoBehaviour
{
    [SerializeField] private AudioListener audioListener;
    //variables visible in the inspector
    public GameObject pipe;
    public float pipeSpawnWait;
    public GameObject PopUp;
    public Text textScore;
    public Text finalScore;
    public Text finalCoin;

    public Text pipeOver;
    //variables not visible in the inspector
    public static bool gameOver;
    public static bool startMenu;
    public AudioSource gameOverAudio;
    GameObject GameOverMenu;
    GameObject StartMenu;

    GameObject Score;
    GameObject BestScore;
    GameObject timeObject;

    public static int score;
    float height;
    float width;

    void Start()
    {
        if(ScreenManager.I != null)
            ScreenManager.I.TurnModeSound();
        PopUp.SetActive(false);
        //set score 0 and timescale 1.5
        score = 0;
        Time.timeScale = 1.5f;

        //search for the menu objects
        StartMenu = GameObject.Find("Start menu");
        // GameOverMenu = GameObject.Find("Game over menu");
        Score = GameObject.Find("Score");
        BestScore = GameObject.Find("Best");

        //show best score on best score object using UI text
        // BestScore.GetComponent<UnityEngine.UI.Text>().text = "Лучший результат: " + PlayerPrefs.GetInt("Best(flappy_cube)");

        //set objects true/false
        // Score.SetActive(false);
        startMenu = true;
        gameOver = false;
        StartMenu.SetActive(true);
        // GameOverMenu.SetActive(false);

        //get screen width/height
        height = (float)Camera.main.orthographicSize * 2.0f;
        width = height * Screen.width / Screen.height;
        
    }

    void Update()
    {
        //Debug.Log(score);
        //if the game is playing, accelerate difficulty and show score
        if (!gameOver && !startMenu)
        {
            textScore.text = "" + (int)(score / 5);
            pipeOver.text = score.ToString();
 //           Score.GetComponent<UnityEngine.UI.Text>().text = "" + score;
            Time.timeScale += Time.deltaTime * 0.01f;
        }
        //if we are game over, display score
        if (gameOver)
        {
            PopUp.SetActive(true);
            finalScore.text = score.ToString();
            finalCoin.text = (score / 5).ToString();

            // StartCoroutine(GameOverSound());

            //           Score.GetComponent<UnityEngine.UI.Text>().text = "Счет: " + score;
        }

        if(Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ScreenManager.I.OpenScene("main_new", "MinGames");
            }
        }
    }

    IEnumerator GameOverSound()
    {
		
        yield return new WaitForSeconds(gameOverAudio.clip.length);
        gameOverAudio.Play();
        PopUp.SetActive(true);
    }

    public void startGame()
    {
        //start game and begin spawning
        StartMenu.SetActive(false);
        // BestScore.SetActive(false);
        startMenu = false;
 //       Score.SetActive(true);
        StartCoroutine(spawnPipes());
        GameObject.Find("Cube").GetComponent<Rigidbody>().isKinematic = false;
    }

    public void endGame()
    {
        //game is over and game over UI is active
		if (gameOver == false) {
			if (WebRequestController.I != null) {
				WebRequestController.I.GameFlappyCompleted (score, null);
			}
		}
        gameOver = true;
        // GameOverMenu.SetActive(true);
        //save highscorev

        if (score > PlayerPrefs.GetInt("Best(flappy_cube)"))
        {
            PlayerPrefs.SetInt("Best(flappy_cube)", score);
        }
    }

    public void quitGame()
    {
        //quit app
        Application.Quit();
    }

    public void restartGame()
    {
        //restart this scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    IEnumerator spawnPipes()
    {
        //spawn in a loop
        while (true)
        {
            Spawn();
            yield return new WaitForSeconds(pipeSpawnWait);
        }
    }

    void Spawn()
    {
        //instantiate pipes at random height and add them to the environment object
        Vector3 randomPos = new Vector3(width / 2 + 1, Random.Range(-height / 4f, height / 4f), 0);
        GameObject newLayer = Instantiate(pipe, randomPos, Quaternion.identity) as GameObject;
        newLayer.transform.parent = GameObject.Find("Environment").transform;
    }
}
