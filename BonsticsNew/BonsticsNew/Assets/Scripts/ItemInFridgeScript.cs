﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ItemInFridgeScript : MonoBehaviour
{
    
    public static event Action<Sprite> OnClickFood;
    private Button button;
    private Sprite sprite;
    private ScreenManager screenManager;

    void Start()
    {
        /*
        GameObject temp = GameObject.FindGameObjectWithTag("ScreenManager");
        if (temp == null)
        {
            Debug.Log("Can't find ScreenManager");
            return; ;
        }
        screenManager = temp.GetComponent<ScreenManager>();
        */
        button = gameObject.GetComponentInChildren<Button>();
        Image image = button.GetComponent<Image>();
        sprite = image.sprite;
        // screenManager = temp.GetComponent<ScreenManager>();

        Button.ButtonClickedEvent clicked = new Button.ButtonClickedEvent();   
        clicked.AddListener(OnClickItem);

        button.onClick = clicked;   
    }

    void OnClickItem()
    {
        ActiveFoodDescriptionScript.I.SetActiveScreen(true);
        // GameObject obj =  GameObject.Find("FoodDescriptionScreen");
        
            if (OnClickFood != null)
                OnClickFood(sprite);
        
        
            // Debug.Log("FoodDescriptionScreen is null");
        
    }
}
