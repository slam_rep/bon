﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothesScript : MonoBehaviour {

    private GameObject current;
    // public LayerMask lm;
    // public ScreenManager screenManager;

    void Start()
    {
      //   ItemScript.ActivateTamagochi += ActivateTamagochi_method;
    }

    private void ActivateTamagochi_method(Item item)
    {
        if (current != null)
        {
            Destroy(current);
        }

        current = Instantiate(item.bonsticModel.gameObject, gameObject.transform, false);
        current.transform.Rotate(0f, 180f, 0f);
        current.layer = 8;
        foreach (Component child in current.GetComponent<Transform>())
        {
            child.gameObject.layer = (int)8;
        }
        current.transform.localScale = new Vector3(10f, 10f, 10f);
        // screenManager.OpenTamagochi();
    }
}
