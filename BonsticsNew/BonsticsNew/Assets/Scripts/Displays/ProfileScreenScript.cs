﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ProfileScreenScript : MonoBehaviour
{

    public InputField nickname;
    public InputField Name;
    public InputField Surname;
    public InputField DateBirth;
    public InputField LoyalityCard;
    public InputField city;
    public Button acceptButton;
    public ScreenManager screenManager;
    public GameObject profileScreen;
	public Text errorInfo;
	public Kakera.PickerController pickerController;

	// Use this for initialization
	void Start () {

        var acceptBUtEvent = new Button.ButtonClickedEvent();
		acceptBUtEvent.AddListener(SendFields);
		acceptBUtEvent.AddListener(acceptBUtEvent_handler);
       // acceptBUtEvent.AddListener(OpenPanel);
	    acceptButton.onClick = acceptBUtEvent;

        acceptButton.gameObject.SetActive(true);
		var nicknameChanged = new InputField.OnChangeEvent();

	    Name.onValueChanged = nicknameChanged;
	    Surname.onValueChanged = nicknameChanged;
	    DateBirth.onValueChanged = nicknameChanged;
	    LoyalityCard.onValueChanged = nicknameChanged;
	    city.onValueChanged = nicknameChanged;
        
        nicknameChanged.AddListener(nicknameChanged_handler);

		Texture2D t = WebRequestController.I.UserAvatar ();
		if (t != null) {
			pickerController.ShowAvatarPhoto (t);
		}

        screenManager = ScreenManager.I;
    }

	void OnEnable () {
		errorInfo.gameObject.SetActive (false);
		FillUserData ();
	}

    void OpenPanel()
    {
        screenManager.OpenPanel(profileScreen);
    }

	void FillUserData () {
		Debug.Log ("change profile screen fill data");
		string fname = PlayerPrefs.GetString ("user_fname");
		string lname = PlayerPrefs.GetString ("user_lname");
		string nick = PlayerPrefs.GetString ("user_nick");
		string cityStr = PlayerPrefs.GetString ("user_city");
		string cardStr = PlayerPrefs.GetString ("user_card");

		nickname.text = nick;
		Name.text = fname;
		Surname.text = lname;
		LoyalityCard.text = cardStr;
		//city.text = cityStr;


	}

    void nicknameChanged_handler(string str)
    {

            if (nickname.text.Length > 5 && Name.text.Length > 3 && Regex.IsMatch(Name.text, @"^[a-zA-ZА-Яа-я].*")
            && Surname.text.Length > 3 && Regex.IsMatch(Surname.text, @"^[a-zA-ZА-Яа-я].*") && Regex.IsMatch(DateBirth.text, @"^\d{1,2}-\d{1,2}-\d{4}") && LoyalityCard.text.Length == 17 && Regex.IsMatch(city.text, @"\D{3,}")
        )
        {
            acceptButton.gameObject.SetActive(true);
        }
    }

     void SendFields()
    {
        string nickname = this.nickname.text;
        string name = this.Name.text;
        string surname = this.Surname.text;
        string dataBirth = this.DateBirth.text;
		string loyalityCard = this.LoyalityCard.text;
        string city = this.city.text;

		acceptButton.interactable = false;
		WebRequestController.WebCB cb = ProfileSaved;
        WebRequestController.I.PostEditProfile(name, nickname, surname, surname, loyalityCard, dataBirth, cb);  
    }

	void ProfileSaved (string status, string data){
		acceptButton.interactable = true;
		bool isError = !status.Equals ("ok");
		errorInfo.gameObject.SetActive (isError);

		if (!isError) {
			Close ();
		}
	}

	void Close () {
		ScreenManager.I.Back();
	}

    void acceptBUtEvent_handler()
    {
//        User user =  GameObject.FindGameObjectWithTag("User").GetComponent<User>();
//        user.NickName = nickname.text;
//        user.Name = Name.text;
//        user.Surname = Surname.text;
//        user.LoyalityCard = this.LoyalityCard.text;
//        user.City = this.city.text;
    }
}
