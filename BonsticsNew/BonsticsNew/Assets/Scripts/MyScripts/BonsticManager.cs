﻿using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Bonstic
{
    public int Id;
    public string Name;
    public string Description;
    public Sprite aTexture;
    public Sprite bTexture;
    public Sprite cTexture;


    // public GameObject bonsticModel;  
    public bool isActive;
}


public class BonsticManager : MonoBehaviour
{
    public bool isAllBonsticsActive;
    public Bonstic[] bonstics;
    public int[] bonsticId;
    public event Action UpdateScreen;
    

    public void SetActive(int id)
    {
        for (int i = 0; i < bonstics.Length; i++)
        {
            if (id == bonstics[i].Id)
            {
                Debug.Log("SetActive(bonstic.Id)");
                bonstics[i].isActive = true;
                break;
            }
        }
    }

    void Start()
    {
        WebRequestController.WebCB cb = ParseBonsticCollection;
        WebRequestController.I.GetMyCollections(cb);
        ScreenManager.I.selectedId =  PlayerPrefs.GetInt("SelectedId");

        if (isAllBonsticsActive)
        {
            ActivateAllBonstics();
        }
    }

    private void ActivateAllBonstics()
    {
        foreach(Bonstic bonstic in bonstics)
        {
            bonstic.isActive = true;
        }
    }

    private void SetBonsticCollection()
    {
        for (int i = 0; i < bonsticId.Length; i++)
        {
            foreach (Bonstic bonstic in bonstics)
            {
                //PlayerPrefs.HasKey(bonstic.Id.ToString()) && PlayerPrefs.GetInt(bonstic.Id.ToString()) == 1
                if (bonstic.Id == bonsticId[i])
                {
                    Debug.Log("SetActive(bonstic.Id)" + bonstic.Id);
                    SetActive(bonstic.Id);
                    if(UpdateScreen != null)
                        UpdateScreen();
                }
            }
        }
    }

    void ParseBonsticCollection(string status, string data)
    {
        if (status != "ok")
        {
            return;
        }
        if (data != null)
        {
            var N = JSON.Parse(data);
            var item = N["collection"].AsArray;
            if (item != null)
            {
                
                bonsticId = new int[item.Count];
                
                for (int i = 0; i < item.Count; i++)
                {
                    bonsticId[i] = item[i]["bonstick"].AsInt;
                }
            }
        }
        SetBonsticCollection();
    }
}
