﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class RegistrationDisplay : MonoBehaviour
{

    
    [Header("REGISTRATION Settings")]
    public Button buttonContinue;

    public InputField EmailField;
    public InputField Login;
    public InputField Password;
    public GameObject NextScreen;
	public GameObject PrevScreen;
    public Text text;

    private bool isCorrectEmail;
    private bool isCorrectLogin;
    private bool isCorrectPassword;
    private bool isCorrect;

    void Start()
    {
        text.gameObject.SetActive(false);
    }


	public void BackClicked () {
		PrevScreen.SetActive (true);
		gameObject.SetActive (false);
	}

    //валидация
    private bool CheckRegsitrationInputs()
    {
        return isCorrect = CheckEmail(EmailField.text) & CheckPassword() & CheckLogin();
    }

    private bool CheckPassword()
    {
        return isCorrectPassword =  Password.text.Length > 5;
    }

    private bool CheckLogin()
    {
        return isCorrectLogin = Login.text.Length > 2;
    }

    private bool CheckEmail(string text)
    {
        isCorrectEmail =  Regex.IsMatch(text,
            @"^[0-9a-zA-Z].*\@[0-9a-z-]{2,}\.[a-z]{2,}$");

        return isCorrectEmail;
    }

    public void GetRegistration()
    {
		print ("btn reg clicked ************************");
        if (!CheckRegsitrationInputs())
        {
            text.gameObject.SetActive(true);
            text.text = "";
            string EmailNotCorrect = "Введите правильно e-mail\n";
            string LoginNotCorrect = "Логин не менее 3 символов\n";
            string PasswordNotCorrect = "Пароль не менее 6 символов\n";

            text.gameObject.SetActive(true);
            text.text = (isCorrectEmail ? "" : EmailNotCorrect) + (isCorrectLogin ? "" : LoginNotCorrect) +
                        (isCorrectPassword ? "" : PasswordNotCorrect);
            return;
        }
        text.text = "Обработка запроса..";

//        GameObject screenManager = GameObject.FindGameObjectWithTag("ScreenManager");
//        screenManager.GetComponent<ScreenManager>().OpenPanel(NextScreen);
        // !!!!   
		print ("try to start request **************************");
		WebRequestController.WebCB cb = RegHandler;
        WebRequestController.I.StartRegistration("", "", Login.text, EmailField.text, Password.text, cb);
		buttonContinue.interactable = false;
    }

	void RegHandler (string status, string data) {
		print ("reg screen handler start ************************");
		buttonContinue.interactable = true;
		if (status == "ok") {
			text.text = "Регистрация прошла успешно";
			NextScreen.SetActive (true);
			gameObject.SetActive (false);
		} else {
			print ("else block called ************************");
			print ("status " + status);
			text.gameObject.SetActive (true);
			text.text = status;
		}
		print ("reg screen handler end *****************************");
	}
		
}
