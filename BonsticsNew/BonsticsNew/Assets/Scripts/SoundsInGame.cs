﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundsInGame : MonoBehaviour
{


	// Use this for initialization
	void Start()
	{
        Button but = GetComponent<Button>();
        var ClickEvent = new Button.ButtonClickedEvent();
        ClickEvent.AddListener(ClickEvent_handler);
        but.onClick = ClickEvent;
	}
	
	// Update is called once per frame
	void ClickEvent_handler()
    {
        ScreenManager.I.ChangeSound();
    }
}
