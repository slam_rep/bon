﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Utility;

public class StripesRotator : MonoBehaviour
{
    public float rotateSpeed;
    private Image img;

	// Use this for initialization
	void Start ()
	{
	    img = GetComponent<Image>();
	    // StartCoroutine(RotateStripes());
	}


    void Update()
    {
        img.transform.Rotate(0f, 0f, rotateSpeed * Time.deltaTime);
    }

    IEnumerator RotateStripes()
    {
        while (true)
        {
            img.transform.Rotate(0f,0f, rotateSpeed * Time.deltaTime);
            
            yield return null;
        }
    }
}
