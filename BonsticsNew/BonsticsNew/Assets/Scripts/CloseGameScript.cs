﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseGameScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var clickEvent = new Button.ButtonClickedEvent();
        clickEvent.AddListener(CloseGame);
	    GetComponent<Button>().onClick = clickEvent;
        Debug.Log("Start");
	}

    void CloseGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
