﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TamagochiScreenScript : MonoBehaviour
{

    public Button but;
    public Image SleepBG;
    public RawImage ModelBons;
	public GameObject BonstickSay;
	public int sayTimer = 20;

    void OnEnable()
    {
        SleepBG.gameObject.SetActive(false);
        ModelBons.gameObject.SetActive(true);
        var onClEv = new Button.ButtonClickedEvent();
        onClEv.AddListener(OnClick_handler);
        but.onClick = onClEv;

		BonstickSay.SetActive (false);
		StartCoroutine (SayCoroutine ());
    }

	IEnumerator SayCoroutine () {
		int timer = sayTimer;
		while (timer > 0) {
			timer--;
			yield return new WaitForSeconds (1);
		}
		BonstickSay.SetActive (true);
	}

	void OnDisable () {
		BonstickSay.SetActive (false);
	}

    void OnClick_handler()
    {
        SleepBG.gameObject.SetActive(true);
        ModelBons.gameObject.SetActive(false);
    }
}
