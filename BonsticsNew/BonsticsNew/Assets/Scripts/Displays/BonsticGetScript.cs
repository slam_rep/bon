﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonsticGetScript : MonoBehaviour
{

    public BonsticManager bonstics;
    [SerializeField] private Image bonsticImage;
    [SerializeField]
    private Text bonsticName;

    public Button AddButton;
    public GameObject screen;
    [SerializeField] private Text Header;
    [SerializeField]
    private Text bonsticDescription;

    private int id;

    void Start()
    {
        
    }

	// Use this for initialization
	void OnEnable ()
	{
	    
        id = ScreenManager.I.activeId;
	    if (ScreenManager.I.isCatch == false)
	    {
	        gameObject.SetActive(false);
	        return;
	    }
        

        if (!PlayerPrefs.HasKey(id.ToString()))
	        PlayerPrefs.SetInt(id.ToString(), 1);

        var AddClicked = new Button.ButtonClickedEvent();
        AddClicked.AddListener(AddClicked_handler);
	    AddButton.onClick = AddClicked;
        
        foreach (Bonstic bonstic in bonstics.bonstics)
	    {
	        if (id == bonstic.Id)
	        {
	            bonsticImage.sprite = bonstic.aTexture;
	            bonsticName.text = bonstic.Name;
                Header.text = bonstic.Name;
                bonsticDescription.text = bonstic.Description;
                bonstics.SetActive(id);
	            ScreenManager.I.isCatch = false;
                if(!PlayerPrefs.HasKey(id.ToString()))
                    PlayerPrefs.SetInt(id.ToString(), 1);
	            break;
	        }
	    }
        
	}

    void AddClicked_handler()
    {
        gameObject.SetActive(false);
    }


    

    // Update is called once per frame
    void Update () {
		
	}
}
