﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonsticPlayScript : MonoBehaviour
{
    public BonsticManager bonManager;
    public Button Play;
    public Button Back;
    public int selectedId;
    public Image bonsticImage;
    public Text bonsticName;
    public Text bonsticHeader;
    public Text bonsticDescription;

	// Use this for initialization
	void Start ()
	{
	    
	}

    void OnEnable()
    {
        selectedId = ScreenManager.I.selectedId;


        /*
        bonsticImage.sprite = bonManager.bonstics[selectedId].aTexture;
        bonsticHeader.text = bonsticName.text = bonManager.bonstics[selectedId].Name;
        bonsticDescription.text = bonManager.bonstics[selectedId].Description;
        */


		Bonstic b = GetBonstikName (selectedId);
		if (b != null) {
			bonsticImage.sprite = b.aTexture;
			bonsticHeader.text = bonsticName.text = b.Name;
			bonsticDescription.text = b.Description;
		}

    }

	Bonstic GetBonstikName (int id){
		foreach (Bonstic b in bonManager.bonstics) {
			if (b.Id == id) {
				return b;
			}
		}
		return null;
	}

	public void OpenTamagochiScene(){
        PlayerPrefs.SetInt("SelectedId", ScreenManager.I.selectedId);
		ScreenManager.I.OpenScene ("Tamagochi");
	}

    public void MakeNotActive()
    {
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
