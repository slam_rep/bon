﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleButtonAlarm : MonoBehaviour
{
    public Image[] buts;
    public bool selected;
    private Button button;


    void Start()
    {
        if (selected)
        {
            buts[1].gameObject.SetActive(true);
            buts[0].gameObject.SetActive(false);
        }
        else
        {

            buts[0].gameObject.SetActive(true);
            buts[1].gameObject.SetActive(false);

        }


        button = GetComponent<Button>();

        var eventClick = new Button.ButtonClickedEvent();
        eventClick.AddListener(ToggleImage);
        button.onClick = eventClick;
    }

    private void ToggleImage()
    {
        buts[0].gameObject.SetActive(!buts[0].IsActive());
        buts[1].gameObject.SetActive(!buts[1].IsActive());
        
    }
}
