﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using Object = UnityEngine.Object;

public class Item
{    
    public Sprite aTexture { get; set; }
    public Sprite bTexture { get; set; }
    public Sprite cTexture { get; set; }
    public string Name { get; set; }
    public string Id { get;  set; }
    public BonsticController bonsticModel;
    public GameObject[] Clothes;
    public bool isActive;

    public Item():this(null, null, null, null)
    {
        
    }

    public Item(Sprite texA, Sprite texB, Sprite texC, string name)
    {
        this.aTexture = texA;
        this.bTexture = texB;
        this.cTexture = texC;
        this.Name = name;
        Clothes = new GameObject[3];
    }
}



public class ItemManager : MonoBehaviour
{
    public GameObject bonsticManager;
    private Bonstic[] bonstics;
    public GameObject[] placeholders;
    public GameObject item;

    public GameObject shelves;

    // private Dictionary<int, Item> items;
    //private Dictionary<KeyPair, string> names;
    //private Dictionary<KeyPair, Item> items;
    public int initialScreen;

    private int screen;
    private Stack<GameObject> itemsOnScreen;
    private ScrollRect scroll;

    private Vector2 pos;

    //private BonsticController[] bonstics;
    // private Object[][] textures;
    /*
    private struct KeyPair
    {
        public int numb_group;
        public int numb_item;

        public string ToString()
        {
            return numb_group + "_" + numb_item;
        }
    }
    */
    public Button[] buttons;

    private Color defaultColor;

    void Start()
    {
        defaultColor = buttons[0].GetComponentInChildren<Text>().color;
//        scroll = shelves.GetComponent<ScrollRect>();
//        pos = scroll.normalizedPosition;
        itemsOnScreen = new Stack<GameObject>();
        screen = initialScreen;
        bonstics = bonsticManager.GetComponent<BonsticManager>().bonstics;
        bonsticManager.GetComponent<BonsticManager>().UpdateScreen += UpdateScren_hanler;
        //items = new Dictionary<KeyPair, Item>();       
        // items = new Dictionary<int, Item>();
        //names = new Dictionary<KeyPair, string>();
        //Object[][] textures = new Object[4][]
        //{

        //    Resources.LoadAll("1", typeof(Sprite)),
        //    Resources.LoadAll("2", typeof(Sprite)),
        //    Resources.LoadAll("3", typeof(Sprite)),
        //    Resources.LoadAll("4", typeof(Sprite))
        //}; 

        //TextAsset[] namesInFile = new[]
        //{
        //    Resources.Load("Names1") as TextAsset,
        //    Resources.Load("Names2") as TextAsset,
        //    Resources.Load("Names3") as TextAsset,
        //    Resources.Load("Names4") as TextAsset
        //};


        //Object[] objs = Resources.LoadAll("BonsticModels", typeof(BonsticController));

        //FillNames(namesInFile);



        //FillItems(textures, objs);

        FillShelves();
        ChangeScreen(1);
        // DoActiveCarrot();
    }

    void UpdateScren_hanler()
    {
        ChangeScreen(screen);
    }

/*
IEnumerator LoadTextures()
{
    ResourceRequest loadTextures1 = Resources.LoadAsync("1", typeof(Sprite));
    ResourceRequest loadTextures2 = Resources.LoadAsync("2", typeof(Sprite));
    ResourceRequest loadTextures3 = Resources.LoadAsync("3", typeof(Sprite));
    ResourceRequest loadTextures4 = Resources.LoadAsync("4", typeof(Sprite));

    while (!loadTextures1.isDone && !loadTextures2.isDone && !loadTextures3.isDone && !loadTextures4.isDone)
    {
        yield return null;
    }

    // textures[1] = loadTextures1.asset as ;
    ;
}
*/

    //public void DoActiveCarrot()
    //{
    //    KeyPair kp = new KeyPair() {numb_group = 3, numb_item = 3};
    //    items[kp].isActive = true;
    //    // FillShelves();
    //}

    public void ChangeScreen(int valueForScreen)
    {
        screen = valueForScreen;
        DestroyItemsOnScreen();
        FillShelves();
//        scroll.normalizedPosition = pos;
        LitButton(valueForScreen);
    }

    void LitButton(int index)
    {
        buttons[index - 1].GetComponentInChildren<Text>().color = new Color(.59f, .74f, .12f);

        ResetButtons(buttons[index-1]);
    }

    void ResetButtons(Button but)
    {
        foreach (var button in buttons)
        {
            if (button == but)
                continue;

            button.GetComponentInChildren<Text>().color = defaultColor;
        }
    }



    private void FillShelves()
    {
        for (int i = 24*(screen-1); i < 24* (screen - 1) + 24; i++)
        {
            GameObject newItem = Instantiate(item, placeholders[i- 24*(screen - 1)].transform, false);
            itemsOnScreen.Push(newItem);

            


            Image[] images = newItem.GetComponentsInChildren<Image>();

            if (bonstics[i].isActive)
            {
                images[1].sprite = bonstics[i].aTexture;
                newItem.GetComponentInChildren<Button>().interactable = true;
            }
            else if (!bonstics[i].isActive)
            {
                images[1].sprite = bonstics[i].bTexture;
                images[1].color = new Color(0,0,0,1);
                newItem.GetComponentInChildren<Button>().interactable = false;
            }          
            // images[1].sprite = items[key].aTexture;
            Text[] texts = newItem.GetComponentsInChildren<Text>();
            texts[0].text = bonstics[i].Name;
            ItemScript currentBon = newItem.GetComponent<ItemScript>();
            Bonstic bon = bonstics[i];
            currentBon.bonstic = bon;
        }
    }



    private void DestroyItemsOnScreen()
    {
        while (itemsOnScreen.Count > 0)
        {
            GameObject obj = itemsOnScreen.Pop();
            Destroy(obj);
        }
    }

    //private void FillItems(Object[][] textures, Object[] objs)
    //{
    //    Regex regex = new Regex(@"^\d_\d{1,2}[a-zа-я]$");

    //    foreach (Object[] t1 in textures)
    //    {
    //        foreach (var t in t1)
    //        {
    //            if (!regex.IsMatch(t.name)) continue;
    //            string strNumberGroup = Regex.Match(t.name, @"^\d(?=_)").Value;
    //            string strNumberTexture = Regex.Match(t.name, @"(?<=_)\d{1,2}").Value;
    //            string lastSymbol = Regex.Match(t.name, @"[a-zа-я]$").Value;
    //            int numberGroup = Convert.ToInt32(strNumberGroup);
    //            int numberTexture = Convert.ToInt32(strNumberTexture);

    //            KeyPair key = new KeyPair() {numb_group = numberGroup, numb_item = numberTexture};

    //            if (items.ContainsKey(key))
    //            {
    //                AddTextureToItems(lastSymbol, key, t as Sprite);
    //            }
    //            else
    //            {
    //                items.Add(key, new Item());
    //                AddTextureToItems(lastSymbol, key, t as Sprite);
    //                items[key].Name = names[key];
    //                items[key].Id = key.ToString();
    //            }
    //        }
    //    }

    //    FillModels(objs);
    //}

    //private void FillModels(Object[] objs)
    //{
    //    foreach (Object obj in objs)
    //    {
    //        string[] substrs = obj.name.Split(new []{"_"}, StringSplitOptions.RemoveEmptyEntries);
    //        KeyPair pair = new KeyPair()
    //        {
    //            numb_group = int.Parse(substrs[0]),
    //            numb_item = int.Parse(substrs[1])
    //        };

    //        items[pair].bonsticModel = obj as BonsticController;
    //    }
    //}

    //private void FillNames(TextAsset[] texAssets)
    //{
    //    Regex pattern = new Regex(@"\d+\.\w+");

    //    for (int i = 0; i < texAssets.Length; i++)
    //    {
    //        MatchCollection matchCollection = pattern.Matches(texAssets[i].text);

    //        foreach (Match match in matchCollection)
    //        {
    //            int number = int.Parse(Regex.Match(match.Value, @"\d+").Value);
    //            string name = Regex.Match(match.Value, @"(?<=\.)\w+").Value;
    //            KeyPair key = new KeyPair() {numb_group = i+1, numb_item = number};

    //            if (names.ContainsKey(key))
    //            {
    //                throw new Exception("Repetition number in file Names");
    //            }
 
    //            names.Add(key, name.ToUpper());
    //        }
    //    }     
    //}

    //private void AddTextureToItems(string lastSymbol, KeyPair key, Sprite tex)
    //{
    //    switch (lastSymbol)
    //    {
    //        case "a":
    //            items[key].aTexture = tex;
    //            break;
    //        case "а":
    //            items[key].aTexture = tex;
    //            break;
    //        case "b":
    //            items[key].bTexture = tex;          
    //            break;
    //        case "c":
    //            items[key].cTexture = tex;
    //            break;
    //        default:
    //            throw new Exception("Invalid ending of texture's name");
    //    }
    //}
}
