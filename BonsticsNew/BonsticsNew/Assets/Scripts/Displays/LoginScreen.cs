﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginScreen : MonoBehaviour {
	
	private bool isCorrect;
	private bool isCorrectLogin;
	private bool isCorrectPassword;

	public GameObject PrevScreen;
	public GameObject NextScreen;
	public Button loginButton;
	public InputField Login;
	public InputField Password;
	public Text text;

	void Start () {
		text.gameObject.SetActive (false);
	}

	private bool CheckAuthInputs()
	{
		return isCorrect = CheckPassword() & CheckLogin();
	}

	private bool CheckPassword()
	{
		return isCorrectPassword =  Password.text.Length > 5;
	}

	private bool CheckLogin()
	{
		return isCorrectLogin = Login.text.Length > 2;
	}

	private void GetAuth()
	{
		if (!CheckAuthInputs())
		{
			text.gameObject.SetActive(true);
			text.text = "";
			string LoginNotCorrect = "Логин не менее 3 символов\n";
			string PasswordNotCorrect = "Пароль не менее 6 символов\n";

			text.gameObject.SetActive(true);
			text.text = (isCorrectLogin ? "" : LoginNotCorrect) + (isCorrectPassword ? "" : PasswordNotCorrect);
			return;
		}

		text.text = "";
  
		WebRequestController.WebCB cb = AuthHandler;
		WebRequestController.I.LoginUser (Login.text, Password.text, cb);
		loginButton.interactable = false;
	}

	void AuthHandler (string status, string data) {
		loginButton.interactable = true;

		if (status == "ok") {
			NextScreen.SetActive (true);
			gameObject.SetActive (false);
		} else {
			text.gameObject.SetActive (true);
			text.text = status;
		}
	}

	public void LoginClicked () {
		GetAuth ();
	}

	public void BackClicked () {
		PrevScreen.SetActive (true);
		gameObject.SetActive (false);
	}
}
