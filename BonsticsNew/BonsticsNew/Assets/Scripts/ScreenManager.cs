﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.UIPanel;
using UnityEngine;
using UnityEngine.SceneManagement;
using VoxelBusters.NativePlugins.Internal;

public class ScreenManager : MonoBehaviour
{

    public string TagOfScreens;

    [Header("Особые экраны")]
    public string initialScreenName;
    public string settingScreenName;
    public string tamagochiName;
    public string miniGameName;
    public string registrationName;
    public string registrationDone;
    public string bonsGet;
    [Header("-------------------")]

	public SetCanvasBounds canvasBounds;
    
    private Stack<string> transitionHistory;
    // !!!
    public int activeId;

    public int selectedId;
    public AudioClip pressButton;
    private AudioSource audioSource;
    public static ScreenManager I { get; private set; }
    public bool isFirstLoad = true;

    //особые экрыны
    private GameObject MiniGame;
	private GameObject initialScreen;
    private GameObject settingScreen;
    private GameObject Tamagochi;

    private Transform[] screens;

    private AsyncOperation asyncOperation;
    public bool isSoundOn = true;

    private AudioListener audioListener;
    public bool isCatch;
    
    void Awake()
    {
        if (I == null)
            I = this;
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(I);
    }

    void Start()
    {
        //  AudioListener.volume = 0f;
        GetScenes();
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();
        if (transitionHistory == null)
            transitionHistory = new Stack<string>();
        OpenPanel(initialScreen, false);
        isCatch = false;
    }

    public void ChangeSound()
    {
        isSoundOn = !isSoundOn;
        TurnModeSound();
    }

    public void TurnModeSound()
    {
        if (isSoundOn)
        {
            AudioListener.volume = 1f;
        }
        else
        {
            AudioListener.volume = 0f;
        }
    }

    void OnEnable()
    {
        /*
        GameObject canvas = GameObject.Find("Canvas");
        if (canvas != null)
        {
            audioListener = canvas.GetComponent<AudioListener>();
        }
        */
    }
 

    /*
    public void ChangeSound()
    {
        isSoundOn = !isSoundOn;
        // audioListener.enabled = isSoundOn;
        AudioListener[] components = Resources.FindObjectsOfTypeAll<AudioListener>();
        if(components != null)
        {
            if(isSoundOn = true)
            {
                components[0].enabled = true;
            }
            else
            {
                foreach(var v in components)
                {
                    v.enabled = false;
                }
            }
        }
    }*/

    private bool GetScenes()
    {
        var canvas = GameObject.FindGameObjectWithTag(TagOfScreens);

        if (canvas == null)
            return false;
        
        List<PanelUI> temp = new List<PanelUI>();
        var childs = canvas.GetComponentsInChildren<PanelUI>(true);
        temp.AddRange(childs);

        if (temp != null)
        {
            List<Transform> transforms = new List<Transform>();
            
            for (int i = 0; i < temp.Count; i++)
            {
                transforms.Add(temp[i].GetComponent<Transform>());
            }
            screens = transforms.ToArray();
        }
         
        if (screens == null)
            return false;

        FindSpecialScreen();
        
        return true;
    }

    private void FindSpecialScreen()
    {
        foreach (var s in screens)
        {
            if (s.name == initialScreenName)
            {
                initialScreen = s.gameObject;
            }
            if (s.name == miniGameName)
            {
                MiniGame = s.gameObject;
            }
            if (s.name == settingScreenName)
            {
                settingScreen = s.gameObject;
            }
            if (s.name == tamagochiName)
            {
                Tamagochi = s.gameObject;
            }
        }
    }

    void Update()
    {
        isFirstLoad = false;
        
        if (Application.platform == RuntimePlatform.Android && SceneManager.GetActiveScene().name == "main_new")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Back();
            }
        }
        
        
        if (asyncOperation != null && asyncOperation.isDone)
        {
            GetScenes();
            I.OpenPanel(initialScreen);
            if (transitionHistory.Peek() == "MinGames")
            {
                transitionHistory.Pop();
                if(transitionHistory.Peek() == "MinGames" && transitionHistory.Count > 0)
                    transitionHistory.Pop();
                initialScreenName = "StartScreen";
                transitionHistory.Push(initialScreenName);
            }
            asyncOperation = null;
        }
    }

    public void OpenTamagochi()
    {
        OpenPanel(Tamagochi);
    }

    public void OpenPanel(GameObject targetScreen)
    {
        if(targetScreen != null)
            OpenPanel(targetScreen, true);
    }

    public void OpenPanel(GameObject targetScreen, bool playAudio)
    {
		RectTransform t = targetScreen.GetComponent<RectTransform> ();
		if (t != null && canvasBounds != null) {
			canvasBounds.panel = t;
		}
        foreach (Transform screen in screens)
        {
            if (screen != targetScreen && SceneManager.GetActiveScene().name == "main_new")
            {
                screen.gameObject.SetActive(false);
            }

            targetScreen.SetActive(true);
            
        }
        if (targetScreen.name != registrationDone && targetScreen.name != registrationName && targetScreen.name != bonsGet)
        {
            transitionHistory.Push(targetScreen.name);           
        }
        if (playAudio)
        {
            audioSource.Play();
        }
        Debug.Log(targetScreen.name + " at function");
    }

    /*
    public void CloseMiniGames()
    {
        asyncOperation = SceneManager.LoadSceneAsync("main_new");
    }
    */
    /*
    public void CloseScene(string sceneName)
    {
        asyncOperation = SceneManager.LoadSceneAsync(sceneName);
    }
    */

    public AsyncOperation OpenScene(string sceneName)
    {
        return OpenScene(sceneName, "StartScreen");
    }

    public AsyncOperation OpenScene(string sceneName, string iniName)
    {
        if (iniName != null && iniName != string.Empty)
            initialScreenName = iniName;
        return asyncOperation = SceneManager.LoadSceneAsync(sceneName);
    }

    

    public void LoadsMinGames(string scene)
    {
        if(transitionHistory.Count > 0)
            initialScreenName = transitionHistory.Peek();
        SceneManager.LoadSceneAsync(scene);
    }

    public void Back()
    {
        if (transitionHistory.Count > 1)
        {
            transitionHistory.Pop();
            var nameScreen = transitionHistory.Pop();
            OpenPanel(screens.First(a => a.name == nameScreen).gameObject);
        }
    }

    public void OpenSetting()
    {
        OpenPanel(settingScreen);
    }

    public void BonsGet(int id)
    {
        StartCoroutine(BonsCatch(id));
    }

    private IEnumerator BonsCatch(int id)
    {
        AsyncOperation asyncOperation =  this.OpenScene("main_new");

        while (!asyncOperation.isDone)
        {
            yield return null;
        }
        BonsticManager bonstic = null;

        foreach (var rootGameObject in SceneManager.GetSceneByName("main_new").GetRootGameObjects())
        {
            if ((bonstic = rootGameObject.GetComponentInChildren<BonsticManager>()) != null)
            {
                break;
            }
        }

        if(bonstic != null)
            bonstic.SetActive(id);
    }
}
