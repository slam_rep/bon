﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Web;
using UnityEngine;
using UnityEngine.UI;
using System.Net;

public class GPSManager : MonoBehaviour
{

    public Text textCatch;
    public float radius;
    public float timeRequestPosition;

    public GameObject Trap;
    private List<StoresData> storesData;
	float longitude = 0f;
	float latitude = 0f;

    private bool isPosition;

    void Start()
    {
		Debug.Log ("START camera");
		textCatch.text = "Загрузка..";
        storesData = WebRequestController.I.storesData;
        

        Trap.SetActive(true);
        
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            textCatch.text = "Включите GPS, чтобы словить Бонстика";
        }
        
        StartCoroutine(CheckUserPosition());
    }

    IEnumerator CheckUserPosition()
    {
        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 40;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(0.5f);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
			latitude = Input.location.lastData.latitude;
			longitude = Input.location.lastData.longitude;
			print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();

		NearestStore();

        yield return new WaitForSeconds(timeRequestPosition);

		StartCoroutine(CheckUserPosition());
    }


    void NearestStore()
    {
		Debug.Log("Check");
		if (longitude == 0 || latitude == 0) {
			return;
		}
		if (WebRequestController.I.storesData == null) {
			return;
		}
        var storesData = WebRequestController.I.storesData;
        double distance = double.MaxValue;
        for (int i = 0; i < storesData.Count; i++)
        {



            var sd = storesData[i];
            var dist = OnlineMapsUtils.DistanceBetweenPointsD(new Vector2(sd.longitude, sd.latitude),
                new Vector2(longitude, latitude));

            if (dist < distance)
                distance = dist;
        }

        distance *= 1000f;

        if (distance < radius)
        {
            isPosition = true;
            if (!Trap.activeSelf)
                Trap.SetActive(true);

            textCatch.text = "Наведи ловушку на Бонстика\nи задержи на 5 секунд,\nчтобы поймать его!";
        }
        else
        {
            if (Trap.activeSelf)
                Trap.SetActive(false);
            textCatch.text = "Бонстика можно словить только в магазинах Евроопт и Брусничка!";
        }

        print(distance);
    }
}
