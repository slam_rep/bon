﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackAsCloseScenes : MonoBehaviour
{
    private Button but;

	// Use this for initialization
	void Start ()
	{
	    but = GetComponent<Button>();
        var clickButtBack = new Button.ButtonClickedEvent();
        clickButtBack.AddListener(CloseScene);
	    but.onClick = clickButtBack;
	}


    void CloseScene()
    {
        ScreenManager.I.OpenScene("main_new");
    }
}
