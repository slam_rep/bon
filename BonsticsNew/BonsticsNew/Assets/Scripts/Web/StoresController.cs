﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using SimpleJSON;
using UnityEngine;

namespace Assets.Scripts.Web
{
    public class StoresController : MonoBehaviour
    {
        public List<StoresData> storesData = new List<StoresData>();
        public Texture2D textureMarker;
        public float scaleMarker;

        public OnlineMaps mainMap;

        public static StoresController I;

        public static Action<string, string> ClickMarker;

    

        void Awake()
        {
            if (I == null)
                I = this;
            
        
            //mainMap.AddMarker()
        }

        void Start()
        {
            storesData = WebRequestController.I.storesData;
            if (storesData != null)
            {
                AddMarker();
            }
        }
        

        private void AddMarker()
        {

            foreach (var data in storesData)
            {
                OnlineMapsMarker marker = new OnlineMapsMarker();
                marker.texture = textureMarker;
                marker.scale = scaleMarker;
                //marker.label = data.id;
                marker.position = new Vector2(data.longitude, data.latitude);
                marker.OnClick += delegate(OnlineMapsMarkerBase b)
                {
                    //Debug.Log("Test");
                    MapScript.mapScript.ShowPopUp(data.address, data.timeWork);
                };
                mainMap.AddMarker(marker);
            }
        }
    }

    public class StoresData
    {
        public string id;
        //координаты
        public float latitude;
        public float longitude;

        public string address;
        public string timeWork;

        public float distance;
    }
}