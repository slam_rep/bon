﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScript : MonoBehaviour
{

    public GameObject loadingScreen;
	public GameObject regScreen;
    public float durationShowScreen;
    public Text text;

    void Start()
    {
//		PlayerPrefs.DeleteKey ("Login");
		TryUserAuthorize ();
    }

	void TryUserAuthorize () {
//		if (PlayerPrefs.HasKey ("Login")) {
//			Debug.Log ("Autorisation");
//			WebRequestController.WebCB cb = AuthHandler;
//			WebRequestController.I.StartAutorisation (cb);
//		} else {
//			Invoke("ShowRegScreen",durationShowScreen);
//		}
		if (WebRequestController.I.HaveToken ()) {
			Login ();
		} else {
			Invoke("ShowRegScreen",durationShowScreen);
		}
	}

	void AuthHandler (string status) {
		if (status.Equals ("ok")) {
			Login ();
		} else {
			ShowRegScreen();
		}
	}

	void Login () {
		loadingScreen.SetActive (true);
		gameObject.SetActive (false);
	}

	void ShowRegScreen () {
		regScreen.SetActive (true);
		gameObject.SetActive (false);
	}
		
}
