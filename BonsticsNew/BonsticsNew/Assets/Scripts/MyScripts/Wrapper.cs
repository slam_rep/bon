﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Wrapper : MonoBehaviour {


    public void ChageSound()
    {
        ScreenManager.I.ChangeSound();
    }

    public void OpenPanel(GameObject obj)
    {
        ScreenManager.I.OpenPanel(obj);
    }

    public void OpenMinigame(string name)
    {
        ScreenManager.I.LoadsMinGames(name);
    }
    /*
    public void CloseScene()
    {
        ScreenManager.I.OpenScene("main_new");
    }
    */
    public void OpenScene(string name)
    {
        ScreenManager.I.OpenScene(name);
    }

    public void Back()
    {
        ScreenManager.I.Back();
    }
}
