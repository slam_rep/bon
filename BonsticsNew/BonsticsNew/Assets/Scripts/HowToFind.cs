﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToFind : MonoBehaviour {

	public Wrapper wrapper;
	public GameObject loaderObject;
	// Use this for initialization
	void Start () {
		loaderObject.SetActive (false);
	}
	
	public void OpenCameraClicked () {
		loaderObject.SetActive (true);
		Invoke ("OpenCameraScene",0.2f);
	}

	void OpenCameraScene () {
		wrapper.OpenScene ("CatchScene");
	}
}
