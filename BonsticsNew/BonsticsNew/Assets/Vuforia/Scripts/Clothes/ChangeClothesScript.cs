﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChangeClothesScript : MonoBehaviour
{
    public static event Action<string> ItemChanged;

    private enum TypeOfItems
    {
        Coaks, Boards, Accessoaries
    }

    private string[] strArr;

    public GameObject[] coaks;

    public GameObject[] boards;

    public GameObject[] accessoaries;

    private GameObject[][] items;

    private TypeOfItems currentSet;

    private int currentIndex;
    private GameObject bonstic;
    private GameObject neck;
    private GameObject foot;
    private bool haveBonstic;
    private Item currentItem;
    private Transform parent;

    void Start()
    {
        strArr = new []{"Плащи", "Доски", "Аксессуары"};

        items = new[] {coaks, boards, accessoaries};
        haveBonstic = false;
        currentSet = TypeOfItems.Coaks;

        ItemChanged(strArr[(int) currentSet]);
        // parent = neck.transform;

        ClothesController.ClothesButtonClicked += ClothesButtonClicked_handler;
        ClothesController.BoardsButtonClicked += BoardsButtonClicked_handler;
        ClothesController.AccessoriesButtonClicked += AccessoriesButtonClicked_handler;
        ClothesController.LeftButtonClicked += LeftButtonClicked_handler;
        ClothesController.RightButtonClicked += RightButtonClicked_handler;
        Tamagochi.ModelToggle += ModelToggle_handler;
        //ItemScript.ActivateTamagochi += ActivateTamagochi_handler;      
    }

    void ModelToggle_handler(Item item)
    {       
        bonstic = transform.GetChild(0).gameObject;
        Debug.Log(bonstic.name);

        BonsticController bc = bonstic.GetComponent<BonsticController>();
        
        neck = bc.Neck;
        Debug.Log(neck.transform.IsChildOf(bonstic.transform));
        foot = bc.Foot;
        Debug.Log(foot.transform.IsChildOf(bonstic.transform));
        haveBonstic = true;
        currentItem = item;
        // PrimerFunc(item);
        parent = neck.transform;
    }

    void PrimerFunc(Item item)
    {

        if (item.Clothes[(int) currentSet] != null && haveBonstic)
        {
            Destroy(item.Clothes[(int)currentSet]);

       }
        if (item.Clothes[0] != null && haveBonstic)
        {
            Destroy(item.Clothes[(int)currentSet]);

        }

        GameObject obj = Instantiate(coaks[1], neck.transform, false);
        obj.transform.Translate(0f, .0f, -.4f);
        obj.transform.localScale = new Vector3(150f, 150f, 150f);
        item.Clothes[(int) currentSet] = obj;

        GameObject fot = Instantiate(boards[1], foot.transform, false);
        obj.transform.Translate(0f, .2f, -.4f);
        obj.transform.localScale = new Vector3(150f, 150f, 150f);
        item.Clothes[0] = fot;
    }

    private void Destroy()
    {
        if (haveBonstic && currentItem.Clothes[(int)currentSet] != null)
        {
            Destroy(currentItem.Clothes[(int)currentSet]);

        }
    }

    /*
    void ActivateTamagochi_handler(Item item)
    {
        bonstic = gameObject.GetComponentInChildren<Transform>().gameObject;
        neck = GameObject.Find("Neck");
        foot = GameObject.Find("Foot");
    }
    */

    void RightButtonClicked_handler()
    {
        currentIndex++;
        if (currentIndex >= items[(int) currentSet].Length)
        {
            currentIndex = 0;
        }
        ToggleBetween();
    }

    void ToggleBetween()
    {        
        Destroy();
        if (items[(int) currentSet][currentIndex] == null)
            return;

        GameObject obj = Instantiate(items[(int)currentSet][currentIndex], parent.position, parent.rotation);
        // 
                
            currentItem.Clothes[(int) currentSet] = obj;
            obj.transform.parent = parent;
        if (currentSet == TypeOfItems.Coaks)
        { 
            obj.transform.localScale = new Vector3(135f, 135f, 135f);
            obj.transform.localPosition = new Vector3(13.5f, -.1f, -3.7f);
            obj.transform.Rotate(0f, 0f, 90.0f);
        }
         if (currentSet == TypeOfItems.Boards)
        {
            obj.transform.localScale = new Vector3(1f, 1f, 1f);
            obj.transform.localPosition = new Vector3(-7.88f,0,-.3f);
            obj.transform.localEulerAngles = new Vector3(-180, -90, 90);
        }
        if (currentSet == TypeOfItems.Accessoaries)
        {
            obj.transform.localScale = new Vector3(.2f, .2f, .2f);
            obj.transform.localPosition = new Vector3(-6.8f, 1.4f, -15.5f);
            obj.transform.localEulerAngles = new Vector3(0, -90, -90);
        }
    }

    void LeftButtonClicked_handler()
    {
        currentIndex--;
        if (currentIndex < 0)
        {
            currentIndex = items[(int) currentSet].Length - 1;
        }
        ToggleBetween();
    }

    void ClothesButtonClicked_handler()
    {
        currentSet = TypeOfItems.Coaks;
        currentIndex = 0;
        parent = neck.transform;
        ItemChanged(strArr[(int)currentSet]);
    }

    void BoardsButtonClicked_handler()
    {
        currentSet = TypeOfItems.Boards;
        currentIndex = 0;
        parent = foot.transform;
        ItemChanged(strArr[(int)currentSet]);
    }

    void AccessoriesButtonClicked_handler()
    {
        currentSet = TypeOfItems.Accessoaries;
        currentIndex = 0;
        parent = neck.transform;
        ItemChanged(strArr[(int)currentSet]);
    }
}
