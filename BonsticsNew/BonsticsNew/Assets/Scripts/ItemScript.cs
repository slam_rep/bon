﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ItemScript : MonoBehaviour
{
    private GameObject Canvas;
    private Button button;
    public Bonstic bonstic;
    public GameObject choosen;
    public GameObject text;


    //private ScreenManager screenManager;
    //private Button button;
    //public Item item;
    //public static event Action<Item> ActivateTamagochi;

    void Start()
    {

        Canvas = GameObject.Find("Canvas1");
        if (Canvas == null)
        {
            Debug.Log("Can't find Canvas");
            return;
        }
        if(Canvas.GetComponent<CollectionCanvasScript>().Soon != null)
            Canvas.GetComponent<CollectionCanvasScript>().Soon.SetActive(false);

        button = GetComponentInChildren<Button>();

        var ClickEvent1 = new Button.ButtonClickedEvent();
        ClickEvent1.AddListener(openSoonScreen);
        button.onClick = ClickEvent1;
        if(bonstic.Id == PlayerPrefs.GetInt("SelectedId"))
        {
            choosen.SetActive(true);
            text.SetActive(false);
        }
        else
        {
            choosen.SetActive(false);
            text.SetActive(true);
        }
        /*
        GameObject temp = GameObject.FindGameObjectWithTag("ScreenManager");
        if (temp == null)
        {
            Debug.Log("Can't find ScreenManager");
            return;
        }
        button = GetComponent<Button>();
        screenManager = temp.GetComponent<ScreenManager>();


        var eventClick = new Button.ButtonClickedEvent();
        */
        /*
        eventClick.AddListener(ActivateTamagochi_method);
        button.onClick = eventClick;
        
        if (item == null || item.bonsticModel == null)
        {
            button.interactable = false;
        }
        */

        //eventClick.AddListener(screenManager.OpenTamagochi);
        //if(button != null && eventClick != null)
        //    button.onClick = eventClick;

    }

    void openSoonScreen()
    {
        // Canvas.GetComponent<CollectionCanvasScript>().Soon.SetActive(true);
        StartCoroutine(OpenSoon());
    }

    IEnumerator OpenSoon()
    {
        ScreenManager.I.selectedId = bonstic.Id;

        yield return new WaitForSeconds(.4f);
        Canvas.GetComponent<CollectionCanvasScript>().Soon.SetActive(true);
    }

    /*
    private void ActivateTamagochi_method()
    {
        if (ActivateTamagochi != null)
            ActivateTamagochi(item);
    }
    */
}
