﻿using UnityEngine;
using System.Collections;

public class CubeMovement : MonoBehaviour
{

    //int visible in the inspector
    public int Jumpforce;

    void Update()
    {
        //if screen is pressed and the game is not finished, add force up
        if (Input.GetMouseButtonDown(0) && !FlappyCube.gameOver && !FlappyCube.startMenu)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().AddForce(Vector3.up * Jumpforce);
        }
        else
        {
            //else, add some extra gravity to the player
            GetComponent<Rigidbody>().AddForce(Physics.gravity / 2 * GetComponent<Rigidbody>().mass);
        }

        //if cube is to low, end game and destroy cube
        if (transform.position.y < -Camera.main.orthographicSize)
        {
            Camera.main.GetComponent<FlappyCube>().endGame();
            StartCoroutine(destroy());
        }
    }

    void OnCollisionEnter()
    {
        //if cube collides with a pipe, also end the game
        Camera.main.GetComponent<FlappyCube>().endGame();
        StartCoroutine(destroy());
    }

    IEnumerator destroy()
    {
        //destroy object after 5 seconds
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
}
