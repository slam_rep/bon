﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Kakera
{
    public class PickerController : MonoBehaviour
    {
        [SerializeField]
        private Unimgpicker imagePicker;

        [SerializeField]
        private MeshRenderer imageRenderer;

        [SerializeField]
        private RawImage photo;
        [SerializeField]
        private RawImage photo1;


        void Awake()
        {
            imagePicker.Completed += (string path) =>
            {
                StartCoroutine(LoadImage(path, imageRenderer));
            };
        }

        public void OnPressShowPicker()
        {
            imagePicker.Show("Select Image", "unimgpicker", 1024);
        }

        private IEnumerator LoadImage(string path, MeshRenderer output)
        {
            var url = "file://" + path;
            var www = new WWW(url);
            yield return www;

            var texture = www.texture;
            if (texture == null)
            {
                Debug.LogError("Failed to load texture url:" + url);
            }
            //texture.Resize(texture.)
            //ttexture.filterMode = ;
            //photo.texture = texture;
            //textureGet = texture;
            //Texture2D t = new Texture2D(texture.width, texture.height);
            //t.SetPixels(texture.GetPixels());
            //sprite.texture.SetPixels(texture.GetPixels());
            var cropSize = 200;

            //texture = texture.GetPixels(0, 0, cropSize, cropSize, 0);
            //texture.Apply();

            texture = B83.TextureTools.TextureTools.ResampleAndCrop(texture, 500, 500);

            photo.texture = texture;
            photo1.texture = texture;
            
            //photo.texture.wrapMode = ;
            //photo1.texture.wrapMode = TextureWrapMode.Repeat;

            WebRequestController.I.SaveAvatar(texture);
        }

		public void ShowAvatarPhoto (Texture2D t){
			photo.texture = t;
			photo1.texture = t;
		}
    }
}