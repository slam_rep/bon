﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchSceneScript : MonoBehaviour
{


    public int Id;
    [SerializeField] private GameObject placeHolder0;
    [SerializeField] private GameObject placeHolder1;

    // Use this for initialization
    void Start () {
	    GameObject bons = Resources.Load("BonsticModels/"+Id) as GameObject;
	    Instantiate(bons, placeHolder0.transform, false);
        Instantiate(bons, placeHolder1.transform, false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
