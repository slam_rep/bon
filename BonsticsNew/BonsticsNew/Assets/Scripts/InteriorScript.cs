﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public enum TypeOfInterior
{
    Wall = 0, Decor, Floor, Carpet
}


public class InteriorScript : MonoBehaviour
{


    //categories
    //decor - 18
    //floor - 16
    //kovrik - 17
    //wallpaper - 15


    public Button[] buts;
    public Image WallInRoom;
    public Image DecorInRoom;
    public Image Floor;
    public Image Carpet;
    public Text NameOfItem;
	public Text priceText;
	public Text coinText;

	public Button purchaseBtn;
	public Button useButton;
	public Button usedItemButton;

	public GameObject popupPurchase;
	public ShopItem _shopItems;

    public Sprite[] walls;
    public Sprite[] decors;
    public Sprite[] floors;
    public Sprite[] carpets;
    private Sprite[][] collection;
    private TypeOfInterior currentType;

	private JSONArray storeItems;
	private JSONArray myItems;

    public int wall;
    public int decor;
    public int floor;
    public int carpet;
    private string[] itemType = {"Обои", "Декор",  "Пол", "Коврик" };
    private Image[] elementsInRoom;
    private int index;

	public delegate void UpdateAllDataDelegate();
	public UpdateAllDataDelegate updateCB;
    Color defaultColor;

	static string Key_Floor = "USED_DECOR_FLOOR";
	static string Key_Wall = "USED_DECOR_WALL";
	static string Key_Decor = "USED_DECOR_DECOR";
	static string Key_Carpet = "USED_DECOR_CARPET";

    void Start()
    {
		popupPurchase.SetActive (false);
        defaultColor = buts[0].GetComponentInChildren<Text>().color;
		elementsInRoom = new Image[] {WallInRoom, DecorInRoom, Floor, Carpet};
        index = 0;
        currentType = TypeOfInterior.Wall;
        collection = new Sprite[][] {walls,decors,floors,carpets};
		LitButton (0);
        /*
        WallInRoom.sprite = walls[wall];
        DecorInRoom.sprite = decors[decor];
        Floor.sprite = floors[floor];
        Carpet.sprite = carpets[carpet];
        */
		UpdateCoins ();
        NameOfItem.text = itemType[(int)TypeOfInterior.Wall];
        ShowElement();
    }

	public void LoadUsedDecors () {
		int wallId = PlayerPrefs.GetInt (Key_Wall);
		int floorId = PlayerPrefs.GetInt (Key_Floor);
		int carpetId = PlayerPrefs.GetInt (Key_Carpet);
		int decorId = PlayerPrefs.GetInt (Key_Decor);

		WallInRoom.sprite = FindSpriteInList (wallId, _shopItems.walls);
		DecorInRoom.sprite = FindSpriteInList (decorId, _shopItems.decors);
		Floor.sprite = FindSpriteInList (floorId, _shopItems.floors);
		Carpet.sprite = FindSpriteInList (carpetId, _shopItems.carpets);
	}

	Sprite FindSpriteInList(int itemID, List<ItemShop>items){
		foreach (ItemShop item in items) {
			if (item.itemId == itemID) {
				return item.sprite;
			}
		}
		return null;
	}

	void UpdateCoins () {
		coinText.text = PlayerPrefs.GetInt (WebRequestController.PPKey_User_Coins).ToString();
	}

    void SetButton()
    {
        if (currentList()[index].sprite == elementsInRoom[(int) currentType].sprite)
        {
            
        }
    }

    public void RefreshSlider(JSONArray items, JSONArray userItems) {
		storeItems = items;
		myItems = userItems;
		ShowElement ();
	}

    public void Right()
	{
        index++;
		if (index >= currentList().Count)
            index = 0;
        ShowElement();
    }


    public void Left()
    {
        index--;
        if (index < 0)
			index = currentList().Count - 1;
        ShowElement();
    }

	List<ItemShop> currentList(){
		if (currentType == TypeOfInterior.Wall) {
			return _shopItems.walls;
		} else if (currentType == TypeOfInterior.Decor) {
			return _shopItems.decors;
		}  else if (currentType == TypeOfInterior.Carpet) {
			return _shopItems.carpets;
		} 
		return _shopItems.floors;
	}

    

    void ShowElement()
    {
		if (elementsInRoom == null) {
			return;
		}
		ItemShop item = currentList()[index];
		ConfigureItem (item);
        Image img = elementsInRoom[(int) currentType];
        img.sprite = item.sprite;
		NameOfItem.text = item.title;
		priceText.text = item.price.ToString();

		UpdateItemButtonState (item);
    }

	void ConfigureItem (ItemShop item){
		if (item == null || storeItems == null) {
			return;
		}
		foreach (JSONNode node in storeItems) {
			if (node ["id"] == item.itemId) {
				item.category = node ["category"];
				item.title = node ["name"];
				item.price = node ["price"];
			}
		}
	}

	bool IsItemPurchased (ItemShop item) {
		if (item.price == 0) {
			return true;
		}
		foreach (JSONNode node in myItems) {
			if (node ["good"] == item.itemId) {
				return true;
			}
		}
		return false;
	}

	void UpdateItemButtonState (ItemShop item) {
		if (item == null)
			return;
		int category = item.category;
		if (category < 1) {
			category = GetCategoryForCurrentTab ();
		}

		bool isPurchased = IsItemPurchased (item);
		int selectedItemId = PlayerPrefs.GetInt (GetKeyForCategory (category));
		bool isUsed = (selectedItemId == item.itemId);

		if (isUsed) {
			usedItemButton.gameObject.SetActive (true);
			purchaseBtn.gameObject.SetActive (false);
			useButton.gameObject.SetActive (false);
		} else if (isPurchased) {
			usedItemButton.gameObject.SetActive (false);
			purchaseBtn.gameObject.SetActive (false);
			useButton.gameObject.SetActive (true);
		} else {
			usedItemButton.gameObject.SetActive (false);
			purchaseBtn.gameObject.SetActive (true);
			useButton.gameObject.SetActive (false);
		}
	}

    

    void LitButton(int pos)
    {
        buts[pos].GetComponentInChildren<Text>().color = new Color(.59f, .74f, .12f);
        ResetButtons(buts[pos]);
    }

    void ResetButtons(Button but)
    {
        foreach (var button in buts)
        {
            if (button == but)
                continue;

            button.GetComponentInChildren<Text>().color = defaultColor;
        }
    }

	public void PressButton(int type)
	{
		LitButton(type);
		currentType = (TypeOfInterior)type;
		NameOfItem.text = itemType[(int) currentType];
	}

	//categories
	//decor - 18
	//floor - 16
	//kovrik - 17
	//wallpaper - 15

	int GetCategoryForCurrentTab () {
		if (currentType == TypeOfInterior.Wall) {
			return 15;
		}
		if (currentType == TypeOfInterior.Decor) {
			return 18;
		}
		if (currentType == TypeOfInterior.Floor) {
			return 16;
		}
		return 17;
	}

	/// <summary>
	/// 
	/// </summary>
	///

	public void PurchaseButtonClicked () {
		popupPurchase.SetActive (true);
	}

	public void PurchaseCancelClicked () {
		popupPurchase.SetActive (false);
	}

	public void PurchaseBtnConfirmed () {
		popupPurchase.SetActive (false);
		ItemShop item = currentList () [index];
		PurchaseItem (item.itemId);
	}

	public void UseCurrentItemClicked () {
		ItemShop item = currentList () [index];
		if (item == null ) {
			return;
		}
		int category = item.category;
		if (category < 1) {
			category = GetCategoryForCurrentTab ();
		}
		string key = GetKeyForCategory(category);

		PlayerPrefs.SetInt (key,item.itemId);
		UpdateItemButtonState (item);
	}

	string GetKeyForCategory(int category){
		string key = "";
		if (category == 15) {
			key = Key_Wall;
		}
		if (category == 16) {
			key = Key_Floor;
		}
		if (category == 17) {
			key = Key_Carpet;
		}
		if (category == 18) {
			key = Key_Decor;
		}
		return key;
	}

	/// <summary>
	/// 
	/// </summary>
	///


    void PurchaseItem (int itemId){
		if (!InternetChecker.I.HasInternet) {
			return;
		}
		purchaseBtn.interactable = false;
		WebRequestController.WebCB cb = PurchaseHandler;
		WebRequestController.I.BuyGOODS (itemId.ToString (),"1", cb);
	}

	void PurchaseHandler (string status, string data){
		purchaseBtn.interactable = true;
		if (status == "ok") {
			UpdateCoins ();
			if (updateCB != null) {
				updateCB ();
			}
		}
	}
}
