﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ImageAndVideoPicker;
using System.IO;

public class PhotoPicker : MonoBehaviour {


	[SerializeField]
	private RawImage photo;
	[SerializeField]
	private RawImage photo1;


	Texture2D texture;

	void OnEnable()
	{
		PickerEventListener.onImageSelect += OnImageSelect;
		PickerEventListener.onImageLoad += OnImageLoad;
	}

	void OnDisable()
	{
		PickerEventListener.onImageSelect -= OnImageSelect;
		PickerEventListener.onImageLoad -= OnImageLoad;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnImageSelect(string imgPath, ImageAndVideoPicker.ImageOrientation imgOrientation)
	{
	//	Debug.Log ("Image Location : "+imgPath);
	//	log += "\nImage Path : " + imgPath;
	//	log += "\nImage Orientation : " + imgOrientation;
	}

	void OnImageLoad(string imgPath, Texture2D tex, ImageAndVideoPicker.ImageOrientation imgOrientation)
	{
		Debug.Log ("****************Image Location : "+imgPath);
//		texture = tex;
//		if (tex == null) {
//			Debug.Log ("missing texture *************");
//		}
//		WebRequestController.I.SaveAvatar(tex);
//		photo.texture = WebRequestController.I.UserAvatar();
//		Debug.Log (tex);
//		Debug.Log ("**** orientation" + imgOrientation);
//
//		photo1.texture = GetImgByPath(imgPath);
//
//		setImgPhoto (imgPath);
//		GUI.DrawTexture(new Rect(20,50,Screen.width - 40,Screen.height - 60), tex, ScaleMode.ScaleToFit, true);
		setImgPhoto (imgPath);
	}

	public void PickPhotoClicked () {
		#if UNITY_ANDROID
		AndroidPicker.BrowseImage(true);
		#elif UNITY_IPHONE
		IOSPicker.BrowseImage(true); // true for pick and crop
		#endif
	}

	Texture2D GetImgByPath(string path) {
		Debug.Log ("************1");
		byte[] b = File.ReadAllBytes (Application.persistentDataPath + path);
		Texture2D t = new Texture2D (800,800);
		t.LoadImage (b);
		Debug.Log ("************bytes size" + b.Length);
		Debug.Log (t);

		return t;
	}

	IEnumerator loadImg (string path){ 
		var url = "file://" + path;
		Debug.Log ("****** url " + url);
		var www = new WWW (url);
		yield return www;
		photo.texture = www.texture;
		photo1.texture = www.texture;
		WebRequestController.I.SaveAvatar (www.texture);
	}

	void setImgPhoto(string path){ 
		StartCoroutine (loadImg (path));
	}
}
