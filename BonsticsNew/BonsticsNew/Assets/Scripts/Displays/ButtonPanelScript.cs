﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPanelScript : MonoBehaviour
{

    public Button[] buttons;

    void Start()
    {
        var ClickEvent1 = new Button.ButtonClickedEvent();
        ClickEvent1.AddListener(ClickEvent_handler1);
        var ClickEvent2 = new Button.ButtonClickedEvent();
        ClickEvent2.AddListener(ClickEvent_handler2);
        var ClickEvent3 = new Button.ButtonClickedEvent();
        ClickEvent3.AddListener(ClickEvent_handler3);
        var ClickEvent4 = new Button.ButtonClickedEvent();
        ClickEvent4.AddListener(ClickEvent_handler4);


        buttons[1].onClick = ClickEvent1;
        buttons[2].onClick = ClickEvent2;
        buttons[3].onClick = ClickEvent3;
        buttons[4].onClick = ClickEvent4;

        ClickEvent_handler1();
    }

    void ClickEvent_handler1()
    {
        buttons[1].GetComponent<RectTransform>().localScale = Vector3.one * 1.5f;
        ResetButtons(buttons[1]);
    }

    void ResetButtons(Button but)
    {
        foreach (var button in buttons)
        {
            if(button == but)
                continue;

            button.GetComponent<RectTransform>().localScale = Vector3.one;
        }
    }

    void ClickEvent_handler2()
    {
        buttons[2].GetComponent<RectTransform>().localScale = Vector3.one * 1.5f;
        ResetButtons(buttons[2]);
    }

    void ClickEvent_handler3()
    {
        buttons[3].GetComponent<RectTransform>().localScale = Vector3.one * 1.5f;
        ResetButtons(buttons[3]);
    }

    void ClickEvent_handler4()
    {
        buttons[4].GetComponent<RectTransform>().localScale = Vector3.one * 1.5f;
        ResetButtons(buttons[4]);
    }
}
