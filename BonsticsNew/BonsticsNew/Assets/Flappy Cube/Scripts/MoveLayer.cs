﻿using UnityEngine;
using System.Collections;

public class MoveLayer : MonoBehaviour {
	
	//variables visible in the inspector
	public float movespeed;
	public bool horizontal;
	
	void Start(){
	//rotate object if you want it to move horizontally
	if(horizontal){
	transform.rotation = Quaternion.Euler(0, 0, 90);
	}	
	}
	
	void Update(){
	//move object up
	transform.Translate(Vector3.up * movespeed * Time.deltaTime);	
	
	//check position of object and horizontal boolean to destroy it after it isn't visible anymore
	if((transform.position.y > Camera.main.orthographicSize && !horizontal) || 
	(horizontal && transform.position.x < -Camera.main.orthographicSize * Screen.width/Screen.height - 1)){
	Destroy(gameObject);
	}
	}
	
	void OnTriggerEnter(){
	//add one to the score if flappy cube object hits trigger
	FlappyCube.score += 1;	
	}
}
