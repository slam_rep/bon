﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CloseBUtScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
         var butClickedEv = new Button.ButtonClickedEvent();
	    GetComponent<Button>().onClick = butClickedEv;
        butClickedEv.AddListener(butClickedEv_handler);

	}

    void butClickedEv_handler()
    {
        ScreenManager.I.OpenScene("main_new", "MinGames");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
