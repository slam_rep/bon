﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShopItemController : MonoBehaviour
{
    public GameObject[] collectionItem;


    public Button left;
    public Button right;


    private int index;
    
    void Start ()
    {
        index = -1;
        Button.ButtonClickedEvent clicked1 = new Button.ButtonClickedEvent();
        clicked1.AddListener(Left);
        left.onClick = clicked1;

        Button.ButtonClickedEvent clicked2 = new Button.ButtonClickedEvent();
        clicked2.AddListener(Right);
        right.onClick = clicked2;

        foreach (var col in collectionItem)
        {
            col.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update ()
    { 
        
	}


    public void Left()
    {
        index++;
        if (index >= collectionItem.Length)
            index = 0;
        ShowElement();
    }

    public void Right()
    {
        index--;
        if (index < 0)
            index = collectionItem.Length - 1;
        ShowElement();
    }

    void ShowElement()
    {
        Debug.Log(index);
        for (int i = 0; i < collectionItem.Length; i++)
        {
            if (i != index)
                collectionItem[i].SetActive(false);
            else
            {
                collectionItem[i].SetActive(true);
            }
        }
    }

}
