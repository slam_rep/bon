﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class vUIDrag : MonoBehaviour
{
    public Camera camera;
    private float OffsetX;
    private float OffsetY;
    public Text textOver;


    private int[] Ids;
    // public int Id;
    [SerializeField]
    private GameObject placeHolder0;
    [SerializeField]
    private GameObject placeHolder1;

    public Transform bonstic;
    public ImageTargetBehaviour image0;
    public ImageTargetBehaviour image1;


    private Vector3 startPosition;
    private bool isDragStart;
    private int id;
    //private ScreenManager screenManager;

    // Use this for initialization
    void Start ()
    {
        Ids = WebRequestController.I._bonsticAvailable;
        int index = Random.Range(0, Ids.Length);
        id = Ids[index];


        GameObject bons = Resources.Load("BonsticModels/" + id) as GameObject;
	    Instantiate(bons, placeHolder0.transform, false);
        Instantiate(bons, placeHolder1.transform, false);
        //screenManager = ScreenManager.I;
        startPosition =  gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isDragStart)
        {
            bool isFound = false;

            if (VuforiaRuntime.Instance.HasInitialized)
            {
                if (image0.CurrentStatus == TrackableBehaviour.Status.TRACKED || image1.CurrentStatus == TrackableBehaviour.Status.TRACKED)
                    isFound = true;
            }

            if ((Input.mousePosition - new Vector3(Display.main.renderingWidth / 2.0f, Display.main.renderingHeight / 2.0f)).magnitude < 150 
                && isFound)
            {
                
                ScreenManager.I.activeId = id;
                ScreenManager.I.isCatch = true;

                #region ToServer

                WebRequestController.WebCB cb = CathBonsticToWeb;
                Debug.Log(id.ToString());
                WebRequestController.I.CatchBonstick(id.ToString(), cb);

                #endregion
                ScreenManager.I.OpenScene("CollectionScene");
                transform.position = startPosition;
                transform.parent.gameObject.SetActive(false);
                isDragStart = false;
                
            }
            
            
            
        }
        //Debug.Log((image.CurrentStatus == TrackableBehaviour.Status.TRACKED)  + " detect ");
	}

    public void BeginDrag()
    {
        isDragStart = true;
        textOver.gameObject.SetActive(false);
        OffsetX = transform.position.x - Input.mousePosition.x;
        OffsetY = transform.position.y - Input.mousePosition.y;
    }

    public void OngDrag()
    {
        transform.position = new Vector3(OffsetX + Input.mousePosition.x, 
            OffsetY+Input.mousePosition.y);
    }

    public void EndDrag()
    {
        isDragStart = false;
        textOver.gameObject.SetActive(true);

        transform.position = startPosition;
       
    }

    void CathBonsticToWeb(string date, string s)
    {

    }

}
