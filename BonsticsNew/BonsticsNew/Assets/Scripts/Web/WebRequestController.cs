﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.Scripts.Web;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
//using VoxelBusters.Utility;

public class WebRequestController : MonoBehaviour
{

	public delegate void Action ();
	public event Action onProfileUpdate;

	public static string PPKey_LastContentResfreshTime = "LastContentRefreshTime";
	public static string PPKey_AllowRefreshContent = "AllowRefreshContent";
	public static string PPKey_GenerallCollectionDescription = "GenerallCollectionDescription";

	public static string PPKey_Bonus_Coin_Card = "Card_bonus_coins";
	public static string PPKey_User_Coins = "user_coin";
	public static string PPKey_PLAYER_ID = "PLAYER_ID";

	public static string PPKey_TOKEN = "TOKEN";

	public static string PPKey_StoreInfoJSON = "StoreInfoJSON";

	public static string PPLogin = "Login";
	public static string PPPassword = "Password";


	public static string PPKey_ProfileInfoJSON = "ProfileInfoJSON";

	public static string PPKey_RatingINT = "Rating";
	public static string PPKey_CoinBalanceINT = "CoinBalance";
	public static string PPKey_TamagochiBonstikID_String = "TamagochiBonstikID";
	public static string PPKey_TamagochiBonstikIDSelected_StringBool = "TamagochiBonstikIDSelected";


	public delegate void WebCB (string str, string text);


    public string Token;   //полученный при регистрации токен.  записывается в PlayerPrefs

    public static WebRequestController I { get; private set; }

    public event Action<bool> DoneRegistration;
    private const string httpprotocol = "http";

    [Space]

    public int PhotoWidth_Of_StoresTradingCenters;
    public int PhotoHeight_Of_StoresTradingCenters;

    //!!!!!!
    public string Password { get; set; }
    public string Email { get; set; }
    public string Login { get; set; }
    public string OneSignaliD { get; set; }

    public JSONNode.ValueEnumerator nodes;
    public int[] _bonsticAvailable;
    public List<StoresData> storesData = new List<StoresData>();

    public bool IsAutorised;

    public bool isNewUser;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (I == null)
            I = this;
        else
            Destroy(gameObject);
        
    }

    void Start()
    {
//        if (!isNewUser)
//        {
//            if (PlayerPrefs.HasKey("Login"))
//            {
//                Debug.Log("Autorisation");
//                StartAutorisation(null);
//            }
//        }

        StartFormStoresTradingCenters();
    }

    #region Registration
  

    public string GetOneSignalPlayerID()
    {
        var status = OneSignal.GetPermissionSubscriptionState();
        
        if (!PlayerPrefs.HasKey(PPKey_PLAYER_ID))
            PlayerPrefs.SetString(PPKey_PLAYER_ID, status.subscriptionStatus.userId);

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            status.subscriptionStatus.userId = "TestingOneSignalId";
            PlayerPrefs.SetString(PPKey_PLAYER_ID, "TestingOneSignalId");
        }

        return status.subscriptionStatus.userId;
    }


    public void StartRegistration(string NameIF, string LastNameIF, string LoginIF, 
		string EmailIF, string PasswordIF, WebCB cb)
    {
		print ("web request controller reg *****************");
        string url = httpprotocol + "://admin.bonsticks.by/api/v1/register/";

        string device_type = "";
        device_type = GetDeviceType();

        Password = PasswordIF;
        Email = EmailIF;

        PlayerPrefs.SetString("Email", Email);

        Login = LoginIF;
        OneSignaliD = GetOneSignalPlayerID();

        StartCoroutine(GetFormRegister(url, cb,
        new KeyValuePair<string, string>("NAME", NameIF),
        new KeyValuePair<string, string>("LAST_NAME", LastNameIF),
        new KeyValuePair<string, string>("LOGIN", LoginIF),
        new KeyValuePair<string, string>("EMAIL", EmailIF),
        new KeyValuePair<string, string>("PASSWORD", PasswordIF),
        new KeyValuePair<string, string>("PLAYER_ID", OneSignaliD),
        new KeyValuePair<string, string>("DEVICE_TYPE", device_type)

        ));

    }

	private IEnumerator GetFormRegister(string url, WebCB cb, params KeyValuePair<string, string>[] data)
    {

        if (data != null)
            url += "?" + string.Join("&", data.Select(kvp => kvp.Key + "=" + kvp.Value).ToArray());

		UnityWebRequest webRequest = UnityWebRequest.Get(url);
//        using (webRequest = UnityWebRequest.Get(url))
//        {

        yield return webRequest.Send();
            //Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.isError ? webRequest.error : "Get complete! " + webRequest.downloadHandler.text));


        Debug.Log(webRequest.downloadHandler.text);

		print (webRequest.downloadHandler.text);

        var N = JSON.Parse(webRequest.downloadHandler.text);

		if (N == null) {
			if (cb != null) {
				cb ("error", webRequest.downloadHandler.text);
			}

		} else {

			string status = N ["status"].Value;

			if (status == "ok") {
				PlayerPrefs.SetString (PPLogin, Login);
				PlayerPrefs.SetString ("Password", Password);
				PlayerPrefs.SetString ("OneSignal", OneSignaliD);
				PlayerPrefs.SetString ("DeviceType", GetDeviceType ());
				PlayerPrefs.Save ();

				if (DoneRegistration != null)
					DoneRegistration (true);
				IsAutorised = true;

				if (Profile.I != null) {
					Profile.I.ParseProfileDate (webRequest.downloadHandler.text);
				}
			}

			Token = N ["token"].Value;        // versionString will be a string containing "1.0"
			Debug.Log (Token);
			PlayerPrefs.SetString (PPKey_TOKEN, Token);

			if (cb != null) {
				if (status == "error") {
					status = N ["error"].Value;
				}

				cb (status, webRequest.downloadHandler.text);
			}
		}
//        }
    }
    #endregion

    #region Autorisation

	public void StartAutorisation(WebCB cb)
    {
        string url = httpprotocol + "://admin.bonsticks.by/api/v1/auth/";


        StartCoroutine(GetFormAutorisation(url, cb,
        new KeyValuePair<string, string>("LOGIN", PlayerPrefs.GetString("Login")),
        new KeyValuePair<string, string>("PASSWORD", PlayerPrefs.GetString("Password")),
        new KeyValuePair<string, string>("PLAYER_ID", PlayerPrefs.GetString("OneSignal")),
        new KeyValuePair<string, string>("DEVICE_TYPE", GetDeviceType()
			)
        ));

    }

	public void LoginUser (string login, string pass,WebCB cb)
	{
		string url = httpprotocol + "://admin.bonsticks.by/api/v1/auth/";

		StartCoroutine(GetFormAutorisation(url, cb,
			new KeyValuePair<string, string>("LOGIN", login),
			new KeyValuePair<string, string>("PASSWORD", pass),
			new KeyValuePair<string, string>("PLAYER_ID", PlayerPrefs.GetString("OneSignal")),
			new KeyValuePair<string, string>("DEVICE_TYPE", GetDeviceType()
			)
		));
	}

	private IEnumerator GetFormAutorisation(string url, WebCB cb, params KeyValuePair<string, string>[] data)
    {
        if (data != null)
            url += "?" + string.Join("&", data.Select(kvp => kvp.Key + "=" + kvp.Value).ToArray());

		UnityWebRequest webRequest = UnityWebRequest.Get(url);

//		using (webRequest = UnityWebRequest.Get(url))
     //   {
        yield return webRequest.Send();

        Debug.Log(webRequest.downloadHandler.text);

        var N = JSON.Parse(webRequest.downloadHandler.text);
		if (N == null) {
			if (cb != null) {
				cb ("error", webRequest.downloadHandler.text);
			}

		} else {

			Token = N ["token"].Value;      
			string status = N ["status"].Value;
			if (status == "ok") {
				IsAutorised = true;
				if (Profile.I != null) {
					Profile.I.ParseProfileDate (webRequest.downloadHandler.text);
				}
			}
			Debug.Log (Token);
			PlayerPrefs.SetString (PPKey_TOKEN, Token);
			PlayerPrefs.Save ();
			if (cb != null) {
				if (status == "error") {
					status = N ["error"].Value;
				}
				cb (status, webRequest.downloadHandler.text);
			}
		}
    }

	public bool HaveToken () {
		string token = PlayerPrefs.GetString (PPKey_TOKEN);
		if (token != null && token.Length > 0) {
			return true;
		}
		return false;
	}

    #endregion

    #region Stores Trading Centers
    public void StartFormStoresTradingCenters()
    {
        string url = httpprotocol + "://admin.bonsticks.by/api/v1/stores/";
        string PhotoResolution = PhotoWidth_Of_StoresTradingCenters.ToString() + "x" + PhotoHeight_Of_StoresTradingCenters.ToString();

        StartCoroutine(GetFormStoresTradingCenters(url, new KeyValuePair<string, string>("PHOTO_RESOLUTION[]", PhotoResolution)));

    }

    private IEnumerator GetFormStoresTradingCenters(string url, params KeyValuePair<string, string>[] data)
    {


        if (data != null)
            url += "?" + string.Join("&", data.Select(kvp => kvp.Key + "=" + kvp.Value).ToArray());

        

        UnityWebRequest webRequest;
        using (webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.Send();
            //Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.isError ? webRequest.error : "Get complete! " + webRequest.downloadHandler.text));


            //Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text));
            //Debug.Log("вот магазины");
            //   MainScript.Instance.StoreInfoJSONString = CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text);
            string webText = webRequest.downloadHandler.text;

            PlayerPrefs.SetString(PPKey_StoreInfoJSON, CorrectRusCharsScript.CorrectRusChars(webText));
            Debug.Log(CorrectRusCharsScript.CorrectRusChars(webText));

            var node = JSON.Parse(webText);
            
			if (node != null) {
				JSONNode.ValueEnumerator nodesCruch = node ["stores"].Values;
				nodes = nodesCruch;
			}

            ParseStoresData();
        }

    }





    private IEnumerator PostFormStoresTradingCenters(string PhotoResolution)
    {
        string url = httpprotocol + "://admin.bonsticks.by/api/v1/stores/";
        WWWForm form = new WWWForm();
        form.AddField("PHOTO_RESOLUTION[]", PhotoResolution);
        UnityWebRequest webRequest = UnityWebRequest.Post(url, form);
        yield return webRequest.Send();
		if (webRequest.isError)
            Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
        else
            Debug.Log("Post complete!");
        Debug.Log(webRequest.downloadHandler.text);
        var N = JSON.Parse(webRequest.downloadHandler.text);
        Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text));


        //MainScript.Instance.StoreInfoJSONString = CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text);

        PlayerPrefs.SetString(PPKey_StoreInfoJSON, CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text));
    }
    #endregion

    public static string GetDeviceType()
    {
        string device_type = "";
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                device_type = "ANDROID";
                break;
            case RuntimePlatform.IPhonePlayer:
                device_type = "IOS";

                break;
            case RuntimePlatform.WSAPlayerX86:
                device_type = "WINDOWS";
                break;

            case RuntimePlatform.WSAPlayerX64:
                device_type = "WINDOWS";
                break;
            case RuntimePlatform.WSAPlayerARM:
                device_type = "WINDOWS";
                break;
            case RuntimePlatform.WindowsEditor:
                device_type = "WINDOWS";
                break;
        }

        return device_type;
    }

	private void ParseProfile(JSONNode profileJson) {
		if (profileJson == null)
			return;
		string fname = profileJson ["name"].Value;
		string lname = profileJson ["last_name"].Value;
		string nickStr = profileJson ["pseudonym"].Value;
		string city = profileJson ["city"].Value;
		string card = profileJson ["loya_card_number"].Value;
		int coin = profileJson["coin"];
	    var bonsticksAvailable = profileJson["bonsticks_available"].AsArray;

        _bonsticAvailable = new int[bonsticksAvailable.Count];

	    for (int i = 0; i < bonsticksAvailable.Count; i++)
	    {
	        _bonsticAvailable[i] = Convert.ToInt32(bonsticksAvailable[i].Value);
            //Debug.Log(_bonsticAvailable[i]);
	    }
	    

        int oldCoinValue = PlayerPrefs.GetInt ("user_coin");
		PlayerPrefs.SetString ("user_fname", fname.Length > 0 ? fname : "");
		PlayerPrefs.SetString ("user_lname", lname.Length > 0 ? lname : "");
		PlayerPrefs.SetString ("user_nick", nickStr.Length > 0 ? nickStr : "");
		PlayerPrefs.SetString ("user_card", card);
        
        PlayerPrefs.SetInt (PPKey_User_Coins, coin);

		//check for lyal card bonus
		if (coin > oldCoinValue && oldCoinValue > 0) {
			PlayerPrefs.SetInt (PPKey_Bonus_Coin_Card,coin-oldCoinValue);
		}

		PlayerPrefs.Save();
		if (onProfileUpdate != null) {
			onProfileUpdate ();
		}
	}

	public void GetUserProfile (WebCB cb) {
		StartCoroutine (GetProfileRoutine (cb));
	}

	private IEnumerator GetProfileRoutine(WebCB cb){
		string url = httpprotocol + "://admin.bonsticks.by/api/v1/profile/";
		WWWForm form = new WWWForm();
		string token = PlayerPrefs.GetString (PPKey_TOKEN);
		form.AddField("TOKEN", /*tokenForTesting*/ PlayerPrefs.GetString(PPKey_TOKEN));

		UnityWebRequest webRequest = UnityWebRequest.Post(url, form);
		yield return webRequest.Send();

		if (webRequest.isError){
			Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
		} else {
			Debug.Log("Post complete!");
		}
	
		Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text));

		string status = "error";
		var N = JSON.Parse(webRequest.downloadHandler.text);
		if (N != null) {
			status = N ["status"].Value;
			if (status == "ok") {
				ParseProfile (N);
			}
		}
		if (cb != null) {
			cb (status, webRequest.downloadHandler.text);
		}
	}


    public void PostEditProfile(string name, string nick,
		string lastName, string secondName, string loyalityCard, string birthDay, WebCB cb)
    {
		StartCoroutine(PostFormEditProfile(name, nick, lastName, secondName, loyalityCard, birthDay, cb));
    }

    private IEnumerator PostFormEditProfile(string name, string nick, 
		string lastName, string secondName, string loyalityCard, string birthDay, WebCB cb)
    {
        string url = httpprotocol + "://admin.bonsticks.by/api/v1/profile/edit/";

        WWWForm form = new WWWForm();

		string token = PlayerPrefs.GetString (PPKey_TOKEN);
        form.AddField("TOKEN", /*tokenForTesting*/ PlayerPrefs.GetString(PPKey_TOKEN));
		if (name != null) {
			form.AddField ("NAME", name);
		}
		if (nick != null) {
			form.AddField ("PSEUDONYM", nick);
		}
		if (lastName != null) {
			form.AddField ("LAST_NAME", lastName);
		}
		if (secondName != null) {
			form.AddField ("SECOND_NAME", secondName);
		}

		if (loyalityCard != null && !loyalityCard.Equals ("")) {
			form.AddField ("LOYA_CARD_NUMBER", loyalityCard);
		}

        UnityWebRequest webRequest = UnityWebRequest.Post(url, form);


        yield return webRequest.Send();

		if (webRequest.isError)
        {
            Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
        }
        else
        {
            Debug.Log("Post complete!");
        }

        Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text));

        var N = JSON.Parse(webRequest.downloadHandler.text);

		if (N == null) {
			if (cb != null) {
				cb ("error", webRequest.downloadHandler.text);
			}
		}

		string status = N ["status"].ToString ().Replace("\"","");
		bool hasError = status.Equals ("error");
		if (!webRequest.isError && !hasError) {
			ParseProfile (N);
		}
		string cardError = N["loya_card_error"];

        Token = N["token"].Value;        // versionString will be a string containing "1.0"
        for (int i = 0; i < N["photo"].Count; i++)
        {
            //GotPhotoURL = N["photo"][i].Value;
        }

        //var name = N["data"]["sampleArray"][2]["name"];// name will be a string containing "sub object"
		if ( cardError != null && cardError.Length > 0) {
			status = cardError;
		}

		if (cb != null) {
			cb (status, webRequest.downloadHandler.text);
		}
    }

	public void BuyGOODS(string goodsId, string count, WebCB cb) {
		StartCoroutine (BuyGOODSCouroutine(goodsId,count,cb));
	}

	private IEnumerator BuyGOODSCouroutine(string goodsId, string count, WebCB cb)
	{

		string token = PlayerPrefs.GetString (PPKey_TOKEN);
//		string goods = "&GOODS=[{\"ID\":" + goodsId + ",\"COUNT\":" + count + "}]";
		string ur1 = "http://admin.bonsticks.by/api/v1/store/buy/";


		WWWForm form = new WWWForm ();
		form.AddField ("TOKEN", token);
		string goodsJSONstr = "[{\"ID\":" + goodsId + ",\"COUNT\":" + count + "}]";
		form.AddField ("GOODS",goodsJSONstr);
		UnityWebRequest webRequest = UnityWebRequest.Post(ur1, form);

		yield return webRequest.Send();


		if (webRequest.isError){
			Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
		}else{
			Debug.Log("Post complete!");
		}

		Debug.Log (webRequest.downloadHandler.text);
		var N = JSON.Parse (webRequest.downloadHandler.text);
		string status = "error";

		if (N != null) {

			status = N["status"];
			int coin = N ["coin"];
			PlayerPrefs.SetInt (PPKey_User_Coins, coin);
		}
		if (cb != null) {

			cb (status, webRequest.downloadHandler.text);
		}
	}

	// get my purchased items
	public void GetUserGOODS (WebCB cb) {
		StartCoroutine (GetUserGoodsRoutine (cb));
	}

	private IEnumerator GetUserGoodsRoutine(WebCB cb){
		string url = httpprotocol + "://admin.bonsticks.by/api/v1/store/goods/";
		WWWForm form = new WWWForm();
		string token = PlayerPrefs.GetString (PPKey_TOKEN);
		form.AddField("TOKEN", PlayerPrefs.GetString(PPKey_TOKEN));

        

        UnityWebRequest webRequest = UnityWebRequest.Post(url, form);
		yield return webRequest.Send();

		if (webRequest.isError){
			Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
		} else {
			Debug.Log("Post complete!");
		}

		Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text));

		string status = "error";
		var N = JSON.Parse(webRequest.downloadHandler.text);
		if (N != null) {
			status = N ["status"].Value;
		}
		if (cb != null) {
			cb (status, webRequest.downloadHandler.text);
		}
	}

	// get store items
	public void GetStoreGOODS (WebCB cb) {
		StartCoroutine (GetStoreGoodsRoutine(cb));
	}

	private IEnumerator GetStoreGoodsRoutine(WebCB cb){
		string url = httpprotocol + "://admin.bonsticks.by/api/v1/store/";
		WWWForm form = new WWWForm();
		string token = PlayerPrefs.GetString (PPKey_TOKEN);
		form.AddField("TOKEN", PlayerPrefs.GetString(PPKey_TOKEN));

		UnityWebRequest webRequest = UnityWebRequest.Post(url, form);
		yield return webRequest.Send();

		if (webRequest.isError){
			Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
		} else {
			Debug.Log("Post complete!");
		}

		Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text));

		string status = "error";
		var N = JSON.Parse(webRequest.downloadHandler.text);
		if (N != null) {
			status = N ["status"].Value;
		}
		if (cb != null) {
			cb (status, webRequest.downloadHandler.text);
		}
	}

	public void GameFlappyCompleted(int score, WebCB cb){
		WebCB profileCB = (string status, string data) =>
		{
			StartCoroutine (GameCompleted (716,score,cb));
		};
			
		GetUserProfile (profileCB);
	}

	public void GamePazzleCompleted(int score, WebCB cb){
		WebCB profileCB = (string status, string data) =>
		{
			StartCoroutine (GameCompleted (786,score,cb));
		};

		GetUserProfile (profileCB);
	}

	IEnumerator GameCompleted(int gameId, int score, WebCB cb) {
		//http://admin.bonsticks.by/api/v1/tasks/completed/?TOKEN=286824794330e0533c6aba4a5ee8ecf9&TASK[]=786&SCORE=3
		string token = PlayerPrefs.GetString (PPKey_TOKEN);
		string task = "&TASK[]=" + gameId.ToString() +"&SCORE=" + score.ToString();
		string url = "http://admin.bonsticks.by/api/v1/tasks/completed/?" + "TOKEN="+ token + task;


		UnityWebRequest webRequest = UnityWebRequest.Get(url);
		yield return webRequest.Send();

		if (webRequest.isError){
			Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
		}else{
			Debug.Log("Post complete!");
		}

		string status = "error";
		var N = JSON.Parse(webRequest.downloadHandler.text);

		if (N != null) {
			status = N ["status"].Value;
			if (status == "ok") {
				int coins = N["coin"];

				PlayerPrefs.SetInt (PPKey_User_Coins,coins);
				PlayerPrefs.Save ();
			}
		}
		if (cb != null) {
			cb (status, webRequest.downloadHandler.text);
		}
	}

	//получить коллекции бонстиков
	public void GetMyCollections (WebCB cb) {
		StartCoroutine (MyCollectionsRoutine(cb));
	}

	IEnumerator MyCollectionsRoutine (WebCB cb){
		string url = httpprotocol + "://admin.bonsticks.by/api/v1/bonsticks/collection/";
		WWWForm form = new WWWForm();
		string token = PlayerPrefs.GetString (PPKey_TOKEN);
		form.AddField("TOKEN", PlayerPrefs.GetString(PPKey_TOKEN));

		UnityWebRequest webRequest = UnityWebRequest.Post(url, form);
		yield return webRequest.Send();

		if (webRequest.isError){
			Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
		} else {
			Debug.Log("Post complete!");
		}

		Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.downloadHandler.text));

		string status = "error";
		var N = JSON.Parse(webRequest.downloadHandler.text);
		if (N != null) {
			status = N ["status"].Value;
		}
		if (cb != null) {
			cb (status, webRequest.downloadHandler.text);
		}
	}

	//дергаем когда словили бонстика чтобы сохранить в профиле
	public void CatchBonstick(string bId, WebCB cb){
		StartCoroutine(CatchBonstickCouroutine(bId,cb));
	}

	private IEnumerator CatchBonstickCouroutine(string bId, WebCB cb)
	{
		string token = PlayerPrefs.GetString (PPKey_TOKEN);
		string goods = "&BONSTICK[]=" + bId.ToString();
		string ur1 = "http://admin.bonsticks.by/api/v1/bonsticks/collection/add/?" + "TOKEN="+ token + goods;

		UnityWebRequest webRequest = UnityWebRequest.Get(ur1);
		yield return webRequest.Send();

		if (webRequest.isError){
			Debug.Log(CorrectRusCharsScript.CorrectRusChars(webRequest.error));
		}else{
			Debug.Log("Post complete!");
		}

		Debug.Log (webRequest.downloadHandler.text);
		var N = JSON.Parse (webRequest.downloadHandler.text);
		string status = "error";
		if (N != null) {
			status = N["status"];
			//			int coin = N ["coin"];
			//			PlayerPrefs.SetInt (PPKey_User_Coins, coin);
		}
		if (cb != null) {
			cb (status, webRequest.downloadHandler.text);
		}
	}

	public void SaveAvatar (Texture2D t){
		string path = Application.persistentDataPath + "/uavatar.png";
		Debug.Log ("SAVE PATH: " + path);
		byte[] bytes = t.EncodeToPNG ();
		File.WriteAllBytes (path,bytes);
	}

	public Texture2D UserAvatar () {
		string path = Application.persistentDataPath + "/uavatar.png";
		Texture2D tex = null;
		byte[] bytes = null;
		if (File.Exists (path)) {
			bytes = File.ReadAllBytes (path);
			tex = new Texture2D (800,800);
			tex.LoadImage (bytes);
		}
		return tex;
	}

    public void ParseStoresData()
    {
        var values = WebRequestController.I.nodes;

        int x = 0;
        foreach (var value in values)
        {
            StoresData store = new StoresData();

            if (x > int.MaxValue)
                break;

            var val = value["coords"].Values;
            var id = value["id"].Value;
            var address = value["address"].Value;
            var time = value["time"].Value;

            store.id = id;
            store.address = address;
            store.timeWork = time;

            int i = 0;

            foreach (var v in val)
            {
                if (i == 0)
                    store.latitude = Convert.ToSingle(v.Value);
                else
                {
                    store.longitude = Convert.ToSingle(v.Value);
                }

                i++;
            }
            storesData.Add(store);
            x++;
        }

        
        Debug.Log(storesData.Count);
    }


}
