﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PPController : MonoBehaviour {



	public static PPController Instance;
//	public static string PPKey_LastContentResfreshTime = "LastContentRefreshTime";
//	public static string PPKey_AllowRefreshContent = "AllowRefreshContent";
//	public static string PPKey_GenerallCollectionDescription = "GenerallCollectionDescription";
//
//
//	public static string PPKey_PLAYER_ID = "PLAYER_ID";
//
//	public static string PPKey_TOKEN = "TOKEN";
//
//	public static string PPKey_StoreInfoJSON = "StoreInfoJSON";
//
//    public static string PPLogin = "Login";
//    public static string PPPassword = "Password";
//
//
//	public static string PPKey_ProfileInfoJSON = "ProfileInfoJSON";
//
//	public static string PPKey_RatingINT = "Rating";
//	public static string PPKey_CoinBalanceINT = "CoinBalance";
//	public static string PPKey_TamagochiBonstikID_String = "TamagochiBonstikID";
//	public static string PPKey_TamagochiBonstikIDSelected_StringBool = "TamagochiBonstikIDSelected";
//

	static DateTime LastContentRefreshTime;

	private void Awake()
	{
		if (Instance==null)
		{
			Instance = this;
		}
		else
		{
		    Destroy(gameObject);
		}

		if (!PlayerPrefs.HasKey(WebRequestController.PPKey_LastContentResfreshTime))
		{
			PlayerPrefs.SetString(WebRequestController.PPKey_LastContentResfreshTime, DateTime.Now.ToLongTimeString());
			PlayerPrefs.SetString(WebRequestController.PPKey_AllowRefreshContent, "true");
		}
		else
		{
			LastContentRefreshTime = DateTime.Parse(PlayerPrefs.GetString(WebRequestController.PPKey_LastContentResfreshTime));
			if ((DateTime.Now - LastContentRefreshTime).TotalHours>24)
			{
				PlayerPrefs.SetString(WebRequestController.PPKey_AllowRefreshContent, "true");
			}
			else
			{
				PlayerPrefs.SetString(WebRequestController.PPKey_AllowRefreshContent, "false");
			}

 		Debug.Log(LastContentRefreshTime.ToLongTimeString());
		}
	}
}
