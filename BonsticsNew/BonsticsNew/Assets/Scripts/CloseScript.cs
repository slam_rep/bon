﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CloseScript : MonoBehaviour {

    private Button button;
    public GameObject popUp;

    void Awake()
    {
        /*
        button = GetComponent<Button>();
        var eventClick = new Button.ButtonClickedEvent();
        eventClick.AddListener(Close);
        button.onClick = eventClick;
        */
    }

    public void Close()
    {

        popUp.SetActive(false);
    }

    public void GetToMainScene()
    {
        ScreenManager.I.OpenScene("main_new", "MinGames");
    }

    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
