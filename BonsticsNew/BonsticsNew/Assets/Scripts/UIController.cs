﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [Header("Displays")]
    public RectTransform[] StartScreen;

    private Button buttonExample;
    private bool isCoorutine;
    private int index;

    void Start ()
    {
        var eventClick = new Button.ButtonClickedEvent();
        eventClick.AddListener(ExampleMethod);
		buttonExample.onClick = eventClick;
	}
	
	
	void Update ()
	{
	    if (!isCoorutine)
	        StartCoroutine(DelayShowPanels());
	    
	}

    IEnumerator DelayShowPanels()
    {
        isCoorutine = true;
        if (index == 0)
            StartScreen[index].gameObject.SetActive(true);
        else
        {
            StartScreen[index].gameObject.SetActive(false);
            
            if (index < StartScreen.Length)
            {
                StartScreen[index].gameObject.SetActive(true);
            }
        }
        index++;
        Debug.Log(index);

        yield return new WaitForSeconds(5);

        
        if (index < StartScreen.Length)
        {
            isCoorutine = false;
        }
    }

    void ExampleMethod()
    {
        
    }
}
