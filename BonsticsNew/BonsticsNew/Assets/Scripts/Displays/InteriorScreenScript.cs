﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using SimpleJSON;




public class InteriorScreenScript : MonoBehaviour
{

    public GameObject[] interior;
    public GameObject tamagochi;
	public RawImage userPhoto;
	public Text coins;
    public Button shopButton;
    public Button Back;
	public InteriorScript slider;
	private JSONArray _myItems;
    private JSONArray _shopItems;
	private JSONArray shopItems
    {
        get { return _shopItems; }
        set
        {
            _shopItems = value;
            RefreshSlider();
        }
    }

    // Use this for initialization
	void Start ()
	{
		Texture2D t = WebRequestController.I.UserAvatar ();
		if (t != null){
			userPhoto.texture = t;
		}
		coins.text = PlayerPrefs.GetInt (WebRequestController.PPKey_User_Coins).ToString();
	    //string path = "interior";

	    //using (StreamReader sr = new StreamReader(path))
	    //{
	        
	    //}
		slider.updateCB = LoadUserItems;

		LoadUserItems();
		SwitchToTamagochi();
	}



    void RefreshSlider()
    {
		slider.RefreshSlider (_shopItems, _myItems);
    }

    void LoadShopItems()
    {
		if (shopItems != null && shopItems.Count > 0) {
			shopItems = _shopItems;
			return;
		}
        WebRequestController.WebCB cb = ParseShopItems;
        WebRequestController.I.GetStoreGOODS(cb);
    }

	void LoadUserItems () {
		WebRequestController.WebCB cb = ParseUserItems;
		WebRequestController.I.GetUserGOODS (cb);
	}

	void ParseUserItems (string status, string data){
		if (status != "ok")
		{
			return;
		}
		if (data != null)
		{
			var N = JSON.Parse(data);
			var mygoods = N["goods"].AsArray;
			if (mygoods != null)
			{
				_myItems = mygoods;
			}
		}
		LoadShopItems ();
	}

    void ParseShopItems(string status, string data)
    {
        if (status != "ok")
        {
            return;
        }
        if (data != null)
        {
            var N = JSON.Parse(data);
            var goods = N["goods"].AsArray;
            if (goods != null)
            {
                shopItems = goods;
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
		if(Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                BackButton();
            }
        }
	}

    public void BackButton()
    {
        ScreenManager.I.OpenScene("CollectionScene");
    }

    public void MiniGameButton()
    {
        ScreenManager.I.OpenScene("main_new", "MinGames");
    }

    public void SwitchToInterior()
    {
        tamagochi.SetActive(false);
        IterateInterior(true);
    }

    public void SwitchToTamagochi()
    {
		slider.LoadUsedDecors ();
        tamagochi.SetActive(true);
        IterateInterior(false);
    }

    private void IterateInterior(bool mode)
    {
        foreach (var item in interior)
        {
            item.SetActive(mode);
        }
    }
}
