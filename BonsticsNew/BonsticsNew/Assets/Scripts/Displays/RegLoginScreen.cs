﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegLoginScreen : MonoBehaviour {

	public GameObject registerScreen;
	public GameObject loginScreen;

	// Use this for initialization
	void Start () {
		
	}
	
	public void RegisterClicked () {
		registerScreen.SetActive (true);
		gameObject.SetActive (false);
	}

	public void LoginClicked () {
		loginScreen.SetActive (true);
		gameObject.SetActive (false);
	}
}
