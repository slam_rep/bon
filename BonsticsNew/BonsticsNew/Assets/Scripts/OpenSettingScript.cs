﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenSettingScript : MonoBehaviour {

    

    void OnEnable()
    {
        Button button = GetComponent<Button>();
        var screenManager = ScreenManager.I;


        var eventClick = new Button.ButtonClickedEvent();
        eventClick.AddListener(ScreenManager.I.OpenSetting);
        button.onClick = eventClick;
    }
}
