﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButMapInCatch : MonoBehaviour
{
    private Button but;

    void Start()
    {
        but = GetComponent<Button>();

        var ClickedEvent = new Button.ButtonClickedEvent();
        ClickedEvent.AddListener(OpenMap);
        but.onClick = ClickedEvent;
    }

    void OpenMap()
    {
        SceneManager.LoadSceneAsync("Map");
    }
}
