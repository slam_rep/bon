﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackAsClose : MonoBehaviour
{

    private ScreenManager screenManager;
    private Button button;
    private Button.ButtonClickedEvent eventClick;

    void Start()
    {/*
        GameObject temp = GameObject.FindGameObjectWithTag("ScreenManager");
        if (temp == null)
        {
            Debug.Log("Can't find ScreenManager");
            return;
        }*/
        button = GetComponent<Button>();
        // screenManager = temp.GetComponent<ScreenManager>();


        eventClick = new Button.ButtonClickedEvent();
        eventClick.AddListener(eventClick_handler);
        button.onClick = eventClick;
    }

    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                eventClick_handler();
            }
        }
    }

    void eventClick_handler()
    {
        ScreenManager.I.OpenScene("main_new", "StartScreen");
    }

    void OnDestroy()
    {
        if (eventClick != null)
            eventClick.RemoveAllListeners();
    }
}
