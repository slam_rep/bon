﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]

public class ItemShop {
	public int itemId;
	public string title;
	public int category;
	public int price;
	public Sprite sprite;
	public bool unlocked;
}

public class ShopItem : MonoBehaviour {
	
	public List<ItemShop> walls = new List<ItemShop>();
	public List<ItemShop> decors = new List<ItemShop>();
	public List<ItemShop> carpets = new List<ItemShop>();
	public List<ItemShop> floors = new List<ItemShop>();
}
