﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoonScreen : MonoBehaviour
{
    public GameObject soonScreen;
    private Button but;
	// Use this for initialization
	void Start ()
	{
	    but = GetComponent<Button>();
        var ClickEvent = new Button.ButtonClickedEvent();
        ClickEvent.AddListener(ClickEvent_handler);
	    but.onClick = ClickEvent;
	}

    void ClickEvent_handler()
    {
        soonScreen.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
