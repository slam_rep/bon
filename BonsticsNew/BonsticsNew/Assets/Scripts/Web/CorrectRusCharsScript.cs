﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


 
 
    public class CorrectRusCharsScript : MonoBehaviour
    {
         
         
        public static string CorrectRusChars(string str)

        {
            str = str.Replace("\\u0410", "А"); str = str.Replace("\\u0430", "а");
            str = str.Replace("\\u0411", "Б"); str = str.Replace("\\u0431", "б");
            str = str.Replace("\\u0412", "В"); str = str.Replace("\\u0432", "в");
            str = str.Replace("\\u0413", "Г"); str = str.Replace("\\u0433", "г");
            str = str.Replace("\\u0414", "Д"); str = str.Replace("\\u0434", "д");
            str = str.Replace("\\u0415", "Е"); str = str.Replace("\\u0435", "е");
            str = str.Replace("\\u0416", "Ж"); str = str.Replace("\\u0436", "ж");
            str = str.Replace("\\u0417", "З"); str = str.Replace("\\u0437", "з");
            str = str.Replace("\\u0418", "И"); str = str.Replace("\\u0438", "и");
            str = str.Replace("\\u0419", "Й"); str = str.Replace("\\u0439", "й");
            str = str.Replace("\\u041a", "К"); str = str.Replace("\\u043a", "к");
            str = str.Replace("\\u041b", "Л"); str = str.Replace("\\u043b", "л");
            str = str.Replace("\\u041c", "М"); str = str.Replace("\\u043c", "м");
            str = str.Replace("\\u041d", "Н"); str = str.Replace("\\u043d", "н");
            str = str.Replace("\\u041e", "О"); str = str.Replace("\\u043e", "о");
            str = str.Replace("\\u041f", "П"); str = str.Replace("\\u043f", "п");
            str = str.Replace("\\u0420", "Р"); str = str.Replace("\\u0440", "р");
            str = str.Replace("\\u0421", "С"); str = str.Replace("\\u0441", "с");
            str = str.Replace("\\u0422", "Т"); str = str.Replace("\\u0442", "т");
            str = str.Replace("\\u0423", "У"); str = str.Replace("\\u0443", "у");
            str = str.Replace("\\u0424", "Ф"); str = str.Replace("\\u0444", "ф");
            str = str.Replace("\\u0425", "Х"); str = str.Replace("\\u0445", "х");
            str = str.Replace("\\u0426", "Ц"); str = str.Replace("\\u0446", "ц");
            str = str.Replace("\\u0427", "Ч"); str = str.Replace("\\u0447", "ч");
            str = str.Replace("\\u0428", "Ш"); str = str.Replace("\\u0448", "ш");
            str = str.Replace("\\u0429", "Щ"); str = str.Replace("\\u0449", "щ");
            str = str.Replace("\\u042a", "Ъ"); str = str.Replace("\\u044a", "ъ");
            str = str.Replace("\\u042b", "Ы"); str = str.Replace("\\u044b", "ы");
            str = str.Replace("\\u042c", "Ь"); str = str.Replace("\\u044c", "ь");
            str = str.Replace("\\u042d", "Э"); str = str.Replace("\\u044d", "э");
            str = str.Replace("\\u042e", "Ю"); str = str.Replace("\\u044e", "ю");
            str = str.Replace("\\u042f", "Я"); str = str.Replace("\\u044f", "я");


            str = str.Replace("\\u0401", "Ё"); str = str.Replace("\\u0451", "ё");






            return str;
        }


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
 
