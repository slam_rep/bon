﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PopUpScript : MonoBehaviour
{
    public Text numberOfCoins;
    public FlappyCube fCube;
    private User user;

    void Start()
    {
        if (numberOfCoins != null)
        {
            /*
            user = GameObject.FindGameObjectWithTag("User").GetComponent<User>();
            if (user != null)
            {
                user.eCoins += 2;
                numberOfCoins.text = user.eCoins.ToString();
            }
            */
        }
    }

    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Close();
            }
        }
    }

    public void Restart()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }

    public void Close()
    {
        // ScreenManager.I.OpenPanel(ScreenManager.I.initialScreen);
        // SceneManager.LoadScene("main_new");
        ScreenManager.I.OpenScene("main_new", "MinGames");

    }
}
