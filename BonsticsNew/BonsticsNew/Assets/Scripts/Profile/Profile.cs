﻿using System;
using System.Collections;
using System.Collections.Generic;
using OneSignalPush.MiniJSON;
using UnityEngine;
using SimpleJSON;
using Newtonsoft.Json;

public class Profile : MonoBehaviour
{
    public static Profile I { get; private set; }

    public string name;
    public string pseudonym;
    public string last_name;
    public string second_name;
    public string login;
    public string email;
    public string phone;
    public string birthday;
    public int coin;
    public int rating;
    public int idTamagotchi;
    public int collection_count;
    public List<int> bonstics_availiable;
    public string loya_card_number;
    public int[] completed_tasks;

    void Awake()
    {
        if (I == null)
        {
            I = this;
        }
        else
        {
            Destroy(gameObject);
        }
        bonstics_availiable = new List<int>();
    }

    public void ParseProfileDate(string jsonData)
    {
       // JsonConvert.DeserializeObject<>()
        //JSONObject jsonObject = JSONObject.

        //jsonObject[]

        //Debug.Log(bonstics_availiable[1]);
    }
}
