﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupLoyalCardBonus : MonoBehaviour {

	public Text coinText;
	void OnEnable(){
		int coin = PlayerPrefs.GetInt (WebRequestController.PPKey_Bonus_Coin_Card);
		coinText.text = coin.ToString ();
		PlayerPrefs.SetInt (WebRequestController.PPKey_Bonus_Coin_Card,0);
	}
		
	public void ClosePopup () {
		gameObject.SetActive (false);
	}

}
