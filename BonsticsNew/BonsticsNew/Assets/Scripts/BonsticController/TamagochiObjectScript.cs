﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TamagochiObjectScript : MonoBehaviour
{

    public GameObject placeholder;


	// Use this for initialization
	void Start () {
		GameObject bons = Resources.Load("BonsticModels/"+ScreenManager.I.selectedId) as GameObject;

	    if (bons != null)
	    {
	        bons =  Instantiate(bons, placeholder.transform, false);
	        Transform[] eachElem = bons.GetComponentsInChildren<Transform>();

	        foreach (var elem in eachElem)
	        {
	            elem.gameObject.layer = 8;
	        }
	    }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
