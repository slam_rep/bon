﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackScript : MonoBehaviour
{

    private ScreenManager screenManager;
    private Button button;
    private Button.ButtonClickedEvent eventClick;

    void Start()
    {/*
        GameObject temp = GameObject.FindGameObjectWithTag("ScreenManager");
        if (temp == null)
        {
            Debug.Log("Can't find ScreenManager");
            return;
        }*/
        button = GetComponent<Button>();
        // screenManager = temp.GetComponent<ScreenManager>();


        eventClick = new Button.ButtonClickedEvent();
        eventClick.AddListener(ScreenManager.I.Back);
        button.onClick = eventClick;
    }

    void OnDestroy()
    {
        if(eventClick != null)
            eventClick.RemoveAllListeners();
    }
}
