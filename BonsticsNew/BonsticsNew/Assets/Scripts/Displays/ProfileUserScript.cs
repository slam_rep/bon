﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileUserScript : MonoBehaviour
{


    public Text numberOfCoins;
    private User user;

	// Use this for initialization
    void Start()
    {
        user = GameObject.FindGameObjectWithTag("User").GetComponent<User>();
        user.changeCoins += delegate { numberOfCoins.text = user.eCoins.ToString(); };
    }

    // Update is called once per frame
	void Update () {
		
	}
}
