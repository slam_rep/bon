﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraLoader : MonoBehaviour {

	public Text loadText;

	public string FirstPartText = "Телепортируем бонстика";
	public string SecondPartText = "Телепортируем бонстика";
	public float textUpdateRate = 0.5f;

	private string currentTitle = null;
	// Use this for initialization
	void Start () {
		currentTitle = FirstPartText;
		StartCoroutine (ChangeText());
	}

	IEnumerator ChangeText () {
		
		int dotsCount = 0;
		float timer = 3f;

		while (timer > 0) {
			string dots = "";

			if (dotsCount == 1) {
				dots = ".";
			}
			if (dotsCount == 2) {
				dots = "..";
			}

			loadText.text = currentTitle + dots;
			yield return new WaitForSeconds (textUpdateRate);
			timer = timer - textUpdateRate;
			dotsCount++;
			if (dotsCount > 2) {
				dotsCount = 0;
			}
		}

		currentTitle = SecondPartText;
		yield return StartCoroutine (ChangeText());
	}

}
