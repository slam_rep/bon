﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WrapperOnlineMap : MonoBehaviour
{

    public OnlineMapsLocationService map;
    public Text textBottom;

    private string startText;
    private bool checkEnableGeodata;

	// Use this for initialization
	void Start ()
	{
	    startText = textBottom.text;
	}
	
	// Update is called once per frame
	void Update () {
	    if (checkEnableGeodata)
	    {
	        if (Input.location.isEnabledByUser)
	        {
	            checkEnableGeodata = false;
	            textBottom.text = startText;
	        }
	    }
	}

    public void OnlineMapUpdate()
    {
        if (Input.location.isEnabledByUser)
        {
            map.UpdatePosition();
        }
        else
        {
            textBottom.text = "Включите доступ к геоданным";
            checkEnableGeodata = true;
        }
    }
}
