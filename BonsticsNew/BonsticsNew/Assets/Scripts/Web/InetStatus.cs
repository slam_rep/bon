﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InetStatus : MonoBehaviour {


	public GameObject panel;

	void Start(){
		panel.SetActive (!InternetChecker.I.HasInternet);
		InternetChecker.I.OnUpdateStatus += InetStatusHandler;
	}

	void OnDestroy (){
		InternetChecker.I.OnUpdateStatus -= InetStatusHandler;
	}

	void InetStatusHandler (bool haveInet) {
		panel.SetActive(!haveInet);
	}
		
}
