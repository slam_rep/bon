﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BonstickSay : MonoBehaviour {

	public Image dialogImage;
	public Text bonstickText;
	public float newPhraseDelay = 5f;

	List<string> sayList;

	// Use this for initialization
	void Start () {
		sayList = new List<string> ();

		sayList.Add ("Расскажи обо мне друзьям");
		sayList.Add ("Грущу, развесели меня");
		sayList.Add ("Купи мне что-нибудь вкусненькое");
		sayList.Add ("Хочу новый коврик");
		sayList.Add ("Хочу новые обои");
		sayList.Add ("Хочу новую одежку");
		sayList.Add ("Когда мы будем ловить мне новых друзей?");
		sayList.Add ("Эй, ты там заснул?");
		sayList.Add ("Давай поиграем что-ли?");
		sayList.Add ("Эх, скучно, а вот у нас на Бонитроне...");
		sayList.Add ("Может тебе анекдотик рассказать? ");
		sayList.Add ("Два бонстика - хорошо, а три лучше)");
		sayList.Add ("В жизни всегда есть место Бонстику");
		sayList.Add ("...А я ему и говорю, мол, ничего ты не знаешь, Бонстик Сноу…");
		sayList.Add ("...Вечерело…");
		sayList.Add ("Классно тут у вас!");
		sayList.Add ("Говорят, что за мини-игры Ешки дают!");
		sayList.Add ("Может там в холодильнике что-то новое появилось?");
		sayList.Add ("А ты все уроки сделал?");
		sayList.Add ("Давай еще помолчим, с тобой так интересно…(сарказм)");
		sayList.Add ("Надо будет себе еще что-нибудь купить");

		StartCoroutine (ChangeTextRoutine ());
	}

	IEnumerator ChangeTextRoutine () {
		System.Random rnd = new System.Random ();
		int index = rnd.Next (sayList.Count);
		bonstickText.text = sayList[index];

		yield return new WaitForSeconds(newPhraseDelay);

		StartCoroutine (ChangeTextRoutine ());
	}

}
