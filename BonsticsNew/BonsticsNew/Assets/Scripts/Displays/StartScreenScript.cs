﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScreenScript : MonoBehaviour
{
	public GameObject loyalPopup;
    public Text textNote;
    public Button MainButton;
    public GameObject PlayNext;
	public GameObject RulesObj;

    private ScreenManager manager;
    private Text text;

    Button.ButtonClickedEvent ClickEvent = new Button.ButtonClickedEvent();

    void Start()
    {
		RulesObj.SetActive (false);
        manager = ScreenManager.I;
		WebRequestController.I.onProfileUpdate += CheckLoyalCardBonus;

        text = MainButton.transform.Find("Text").gameObject.GetComponent<Text>();
        MainButton.onClick = ClickEvent;

        ClickEvent.AddListener(PlayNextOpen);
        textNote.text = "Играй со своим бонстиком";

		WebRequestController.I.GetUserProfile (null);
    }

	void OnDestroy () {
		WebRequestController.I.onProfileUpdate -= CheckLoyalCardBonus;
	}

    void PlayNextOpen()
    {
//        manager.OpenPanel(PlayNext);
		manager.OpenScene("Tamagochi");
    }

	public void CollectionsClicked () {
		ScreenManager.I.OpenScene("CollectionScene");
	}

	public void SettingsClicked () {
		ScreenManager.I.OpenSetting();
	}

	public void RulesClicked () {
		RulesObj.SetActive (true);
	}


	void CheckLoyalCardBonus () {
		int bonus = PlayerPrefs.GetInt (WebRequestController.PPKey_Bonus_Coin_Card);
		if (bonus > 0) {
			Invoke ("ShowLoyalPopup",1f);
		}
	}

	void ShowLoyalPopup () {
		loyalPopup.SetActive (true);
	}

}
