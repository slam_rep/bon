﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegDone : MonoBehaviour {

	public GameObject nextScreen;

	public void NextClicked () {
		nextScreen.SetActive (true);
		gameObject.SetActive (false);
	}
}
