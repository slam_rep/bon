﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGames : MonoBehaviour
{

    public GameObject PopUp;
    public bool IsActivePopUp;

    void Start()
    {
        if(PopUp != null)
            PopUp.SetActive(false);
    }

    void Update()
    {
        if(IsActivePopUp)
            PopUp.SetActive(true);
    }
}
