﻿using UnityEngine;
using UnityEngine.UI;


public class MapScript : MonoBehaviour
{
    public GameObject popUp;
    public Text workTime;
    public Text address;
    public static MapScript mapScript { get; private set; }

    public GameObject ARCamera;
    public GameObject Camera;

    private MapScript()
    {
        mapScript = this;
    }

    // Use this for initialization
    void Start () {
        mapScript.popUp.SetActive(false);
    }
	
    // Update is called once per frame
    void Update () {
        if(Input.GetKeyDown(KeyCode.W))
            mapScript.ShowPopUp("laal", "121");

        if(Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ScreenManager.I.OpenScene("main_new", "StartScreen");
            }
        }
    }

    public void ShowPopUp(string address, string workTime)
    {
        mapScript.popUp.SetActive(true);
        mapScript.address.text = address;
        mapScript.workTime.text = workTime;
    }
    /*
    void OnEnable()
    {
        // ARCamera.SetActive(false);
        Camera.SetActive(true);
    }

    void OnDisable()
    {
        // ARCamera.SetActive(true);
        Camera.SetActive(false);
    }
    */
}

