﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InternetChecker : MonoBehaviour {

	public delegate void StatusUpdateHandler(bool haveInet);
	public event StatusUpdateHandler OnUpdateStatus;

	public static InternetChecker I { get; private set; }
	private bool _hasInternet = true;
	public bool HasInternet { 
		set {
			if (_hasInternet != value) {
				_hasInternet = value;
				if (OnUpdateStatus != null) {
					OnUpdateStatus (value);
				}
			}
		} 
		get {
			return _hasInternet;
		}
	}
		

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
		if (I == null)
			I = this;
		else
			Destroy(gameObject);
	}


	void Start () {
		StartCoroutine (CheckInet());
	}
	
	IEnumerator CheckInet () {
		while (true) {
			WWW url = new WWW ("http://google.com");
			yield return url;

			HasInternet = url.error == null;
//			Debug.Log("inet status" + (HasInternet ? "ok" : "disconnected"));
			yield return new WaitForSeconds (5f);
		}
	}
		
}
