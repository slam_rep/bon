﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileUI : MonoBehaviour
{

    public Text coins;
    public Text name;
    public Texture2D photo;
	public Text profileName;
	public RawImage profileImage;

	// Use this for initialization
	void Start () {
		int coinsCount = PlayerPrefs.GetInt ("user_coin");
		coins.text = coinsCount.ToString ();

		Texture2D t = WebRequestController.I.UserAvatar ();
		if (t != null) {
			profileImage.texture = t;
		}
	}

	void OnEnable () {
		FillUserData ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FillUserData () {
		Debug.Log ("fill user profile");
		string fname = PlayerPrefs.GetString ("user_fname");
		string lname = PlayerPrefs.GetString ("user_lname");
		profileName.text = fname + " " + lname;
	}
}
